package com.fff.zest;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.eclipse.zest.core.widgets.Graph; 
import org.eclipse.zest.core.widgets.GraphConnection; 
import org.eclipse.zest.core.widgets.GraphNode; 
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles; 
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm; 
import org.eclipse.swt.SWT; 
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout; 
import org.eclipse.swt.widgets.Display; 
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Shell; 
import org.omg.CORBA.PRIVATE_MEMBER;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.Anime;
import com.fff.anime.CV;
import com.fff.anime.Charactor;
import com.fff.anime.Director;
import com.fff.login.Login;
import com.fff.panel.ResultSetUsedInZest;
//import com.mysql.jdbc.ResultSet;
import com.fff.panel.newrecord.NewAnime;
import com.mysql.jdbc.PreparedStatement;


// This class handlers the analysis of input resultset from Database and draws
// the output graph.
 public class GraphManager  extends JFrame{
	 final String CVTYPE = "CV";
	 final String DIRECTORTYPE = "Director";
	 final String TYPETYPE = "Type";
	 final String ANIMETYPE = "Anime";
	 Display display;
	  public Shell shell;
	 public Graph graph;
	 public GraphManager() {
		 initial();
	 }
	 // This method initializes the display and shell, and add a listener to the
	 // graph.
	 public void initial(){
		 display = new Display(); 
         shell = new Shell(display); 
         shell.setText("First Zest Program Demo"); 
         shell.setLayout(new FillLayout()); 
         
         shell.setSize(800, 800); 
         graph = new Graph(shell, SWT.NONE); 
		 // This listener will handler the click event on the node. It will do
		 // another query, add the result to the original graph, and pop up a
		 // dialog showing the detail of the node.
         graph.addSelectionListener(new SelectionAdapter() {
        	 @Override 
        	 public void widgetSelected(SelectionEvent e){
        		 
	    		 List selection = ((Graph) e.widget).getSelection(); 
	    		 
	             if (selection.size() == 1) { 
	            	 
	                    Object o = selection.get(0); 
	                    if (o instanceof GraphNode && !((GraphNode) o).isSelected()) {
	                    	
	                    	GraphNode node = (GraphNode) o;
	                    	String nodeType = (String) node.getData("type");
	                    	String keyword = (String)node.getData("keyword");
	                    	if(nodeType.equals(CVTYPE)){
	                    		
	                    		try {
									drawCVSearch(CV.query(keyword));
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
	                    			
	                    	} else if(nodeType.equals(DIRECTORTYPE)){
	                    		try {
									drawDirectorSearch(Director.query(keyword));
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
	                    		
	                    	} else if(nodeType.equals(ANIMETYPE)){
	                    		try {
									drawAnimeSearch(Anime.query(keyword));
								} catch (SQLException e1) {
									e1.printStackTrace();
								}
	                    	} else if(nodeType.equals(TYPETYPE)){
	                    		try{
		                    		String typeId = " ";
		                    		ResultSet[] rs = new java.sql.ResultSet[2];
									PreparedStatement query[] = new PreparedStatement[2];
									query[0] = ConnectDatabase.getStatement("select * from animetype where type = '"+keyword+"'");
									rs[0] = query[0].executeQuery();
									rs[0].next();
										 typeId = rs[0].getString(2);	       
									      
									query[1] = ConnectDatabase.getStatement("select * from japaneseanime where japaneseanime.type = '"
											+typeId+"'");
									rs[1] = query[1].executeQuery();
		                    		drawTypeSearch(rs);
	                    		}catch (Exception e1) {
									e1.printStackTrace();
								}
	                    	}
	                    	System.out.println((String)node.getData("image"));
	                    	ImageIcon imageIcon = new ImageIcon("img\\"+(String)node.getData("image"));
	                    	
			   	             graph.setLayoutAlgorithm(new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true); 

			   	             showDialog((String)node.getData("type")+":"+(String)node.getData("keyword"),(String)node.getData("content"), imageIcon);


	                    } 

	             }

	            
	             
        	 }

			 // This method will pop up a dialog showing given information.
			private void showDialog(String title,String s, ImageIcon imageIcon) {
	   	        JScrollPane scrollPane = new JScrollPane(new JLabel(s));  
	   	        
	   	        scrollPane.setPreferredSize(new Dimension(500,500));  
	   	        Object message = scrollPane;  
	   	          
	   	   
	   	        JTextArea textArea = new JTextArea(s);  
	   	        textArea.setLineWrap(true);  
	   	        textArea.setWrapStyleWord(true);  
	   	        textArea.setMargin(new Insets(5,5,5,5));  
	   	        scrollPane.getViewport().setView(textArea);  
	   	        
	   	        message = scrollPane;  
	   	        JOptionPane pane = new JOptionPane(message, JOptionPane.PLAIN_MESSAGE);
	   	        pane.setIcon(imageIcon);
	   	        pane.setMessage(message);
	   	        
	   	        JDialog dialog =pane.createDialog(title);
	   	        dialog.setAlwaysOnTop(true);
	   	        dialog.setVisible(true);
	   	          
				
			}
         });
         
     }
	 // This method will open a window showing the search result graph.
	 public void openGraph(){

			 graph.setLayoutAlgorithm(new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true); 
			 shell.open(); 
			 graph.redraw();
			   while (!shell.isDisposed()) { 
			          if(!display.readAndDispatch()) { 
			                 display.sleep();
		          }
			      
			   }
			   System.out.println(graph.isDisposed());
			   display.close();
			   System.out.println(graph.isDisposed());
			   shell.dispose();
			   System.out.println(graph.isDisposed());
		
	   
	 }
	 
	 
	//This method checks duplicate nodes.
	   public GraphNode findGraphNode(String keyword,String type){
		   List<GraphNode> nodes = graph.getNodes();
		   for (GraphNode n : nodes) {
			   String k = (String)n.getData("keyword");
			   String t = (String)n.getData("type");
			   if (k.equals(keyword) && type.equals(t)) {
				   return n;
			   }
		   }
		return null;
	   }
	   // This method checks duplicate connections.
	   public boolean hasConnenction(GraphNode n1, GraphNode n2){
		   List<GraphConnection> connections = graph.getConnections();
		   for (GraphConnection graphConnection : connections) {
			GraphNode destination = graphConnection.getDestination();
			GraphNode source = graphConnection.getSource();
			if((destination.equals(n1)&&source.equals(n2))||(destination.equals(n2)&&source.equals(n1))){
				return true;
			}
		}
		   return false;
	   }
	   // Given a result set, create a CV node.
	   public GraphNode createCV(ResultSet rs) throws SQLException{
		   return createCV(rs.getString(2),rs.getString(5),rs.getString(8));
	   }
	   
	   // Given all information needed by CV node and create the CV node.
	   public GraphNode createCV(String name,String url,String discription){
		   GraphNode cvNode = findGraphNode(name,CVTYPE);
		   if(cvNode==null){
			   cvNode = new GraphNode(graph, 
					   SWT.NONE);
			   cvNode.setNodeStyle(cvNode.getNodeStyle() & ~ZestStyles.NODES_HIDE_TEXT); 
			   
			   cvNode.setData("title",name);
			   cvNode.setData("content",discription);
			   cvNode.setData("image",url);
			   cvNode.setData("keyword",name);
			   cvNode.setData("type",CVTYPE);
			   
			   Image cvImage = new Image(display,"img\\blank.png");
			   cvNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("CV:\n"+name, 0, 0);
			   System.out.println("cv info: "+cvNode.getText());
		   }
		   return cvNode;

	   }

	//Given a result set, create a director node.
	public GraphNode createDirector(ResultSet rs) throws SQLException{
		   return createDirector(rs.getString(2),rs.getString(5),rs.getString(8));
	   }
	   // Given all informatino needed by director node and create the director node.
	   public GraphNode createDirector(String name,String url, String discription){
		   GraphNode dNode = findGraphNode(name, DIRECTORTYPE);
		   if(dNode==null){
			   dNode = new GraphNode(graph, SWT.NONE);
			   dNode.setData("title",name);
			   dNode.setData("content",discription);
			   dNode.setData("image",url);
			   dNode.setData("keyword",name);
			   dNode.setData("type",DIRECTORTYPE);
			   Image cvImage = new Image(display,"img\\blank.png");
			   dNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("Director:\n"+name, 0, 0);
		   }
		   return dNode;
	   }
	   
	   // Given a result set, create a type node.
	   public GraphNode createType(ResultSet rs) throws SQLException{
		   return createType(rs.getString(2));
	   }
	   
	   // Given the typename, create a type node.
	   public GraphNode createType(String typeName){
		   GraphNode tNode = findGraphNode(typeName, TYPETYPE);
		   if(tNode==null){
		   tNode = new GraphNode(graph, SWT.NONE);
		   tNode.setData("title","TYPE");
		   tNode.setData("content",typeName);
		   tNode.setData("keyword", typeName);
		   tNode.setData("type",TYPETYPE);
		   Image cvImage = new Image(display,"img\\blank.png");
		   tNode.setImage(cvImage);
		   GC gc = new GC(cvImage);
		   gc.drawText("Type:\n"+typeName, 0, 0);
		   }
		   return tNode;
		   
	   }
	   
	   // Given a result set, create a anime node.
	   public GraphNode createAnime(ResultSet rs) throws SQLException{
		   
		   String name = rs.getString(2);
		   String startDate = rs.getString(3);
		   String endDate = rs.getString(4); 
		   String discription = rs.getString(5);
		   String type = rs.getString(6);
		   String imgUrl = rs.getString(7);
		   return createAnime(name,startDate,endDate,discription,type,imgUrl);
		   
	   }
	   
	   // Given all information needed by the anime node, create an anime node.
	   public GraphNode createAnime(String name,String startDate, String endDate,String discription,String type,String imgUrl){
		   GraphNode aNode = findGraphNode(name, ANIMETYPE);
		   if(aNode==null){
			   aNode = new GraphNode(graph, 
					   SWT.NONE);
			   //Image dImage = new Image(display,imgUrl);
			   //dNode.setImage(dImage);
			   if(endDate==null){
				   endDate="present";
			   }
			   String text = name+'\n'+type+'\n'+startDate+"-"+endDate+'\n'+discription;
			   //aNode.setText(text);
			   aNode.setData("title",name);
			   aNode.setData("content",name+'\n'+type+'\n'+startDate+"-"+endDate+'\n'+discription);
			   aNode.setData("keyword",name);
			   aNode.setData("image", imgUrl);
			   aNode.setData("type",ANIMETYPE);
			   Image cvImage = new Image(display,"img\\blank.png");
			   aNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("Anime:\n"+name, 0, 0);
			  
		   }
		   return aNode;
		   
	   }
	   
	   // Create connections between CV node and Anime node if there isn't one.
	   private void createCVAnimeCharacterConnections(GraphNode cvNode,
			ResultSet animeCharacterRs) throws SQLException {
		while(animeCharacterRs.next()){
			GraphNode animeNode = createAnime(animeCharacterRs);
			
			if(!hasConnenction(cvNode, animeNode)){
				createCVAnimeConnection(cvNode, animeNode, animeCharacterRs.getString(10));
			}
		}
		
	}
	// Create connections between CV node and Anime node.
	   public GraphConnection createCVAnimeConnection(GraphNode cvNode,GraphNode animeNode, String characterDiscription){
           GraphConnection cvAnimeConnection = new GraphConnection(graph, SWT.NONE, cvNode, animeNode);
           //cvAnimeConnection.setImage(new Image(display,characterURL));
           cvAnimeConnection.setText(characterDiscription);
           //cvAnimeConnection.setImage(new Image(display, ""));
           return cvAnimeConnection;
	   }
	   
	   // Create connections between Type node and Anime node if there isn't one
       private void createTypeAnimeConnections(GraphNode typeNode,
			ResultSet resultSet) throws SQLException {
		while(resultSet.next()){
			GraphNode animeNode = createAnime(resultSet);
			if(!hasConnenction(typeNode, animeNode)){
				createTypeAnimeConnection(typeNode, animeNode);
			}
		}
		
	}
	// Create connections between Type node and Anime node.
	   public GraphConnection createTypeAnimeConnection(GraphNode typeNode,GraphNode animeNode){
		   GraphConnection typeAnimeConnection = new GraphConnection(graph, SWT.NONE, typeNode, animeNode);
		   return typeAnimeConnection;
	   }
	   
	   // Create connections between Anime node and CV node if there isn't one.
	   public void createAnimeCVConnections(GraphNode animeNode,ResultSet cvCharacterRS) throws SQLException{
		  
		   while(cvCharacterRS.next()){
			   GraphNode cvNode = createCV(cvCharacterRS);   
			   if(!hasConnenction(animeNode, cvNode)){
				   createAnimeCVConnection(animeNode, cvNode,  cvCharacterRS.getString(11));
			   }
		   }
		   
	   }
		// Create connections between Anime node and CV node.
	   public void createAnimeCVConnection(GraphNode animeNode,GraphNode cvNode, String characterDiscription){
           GraphConnection animeCVConnection = new GraphConnection(graph, SWT.NONE, animeNode, cvNode);
           //animeCVConnection.setImage(new Image(display,characterURL));
           animeCVConnection.setText(characterDiscription);
           //return animeCVConnection;
	   }

	   // Create connections between Anime node and Director node.
	   public GraphConnection createAnimeDirectorConnection(GraphNode animeNode,GraphNode directorNode){
           GraphConnection animeDirectorConnection = new GraphConnection(graph, SWT.NONE, animeNode, directorNode);
           return animeDirectorConnection;
	   }
	   
	   // Create connections between Director node and Anime node if there isn't one.
	   private void createDirectorAnimeConnections(GraphNode directorNode,
			ResultSet resultSet) throws SQLException {
		while(resultSet.next()){
			GraphNode animeNode = createAnime(resultSet);
			if(!hasConnenction(directorNode, animeNode)){
				createDirectorAnimeConnection(directorNode, animeNode);
			}
		}
		
	}
	   // Create connections between Director node and Anime Node.
	   public GraphConnection createDirectorAnimeConnection(GraphNode directorNode,GraphNode animeNode){
           GraphConnection directorAnimeConnection = new GraphConnection(graph, SWT.NONE, directorNode, animeNode);
           return directorAnimeConnection;
	   }
	   
	   // Add nodes and connections from an Anime Search to the graph
	   public void drawAnimeSearch(ResultSet[] rs) throws SQLException{
		   ResultSet animeRs = rs[0];
		   ResultSet cvCharacterRs = rs[1];
		   ResultSet directorRs = rs[2];
		   animeRs.next();
		   if(!cvCharacterRs.isBeforeFirst())
		   {
			   if(!directorRs.isBeforeFirst()){
				   GraphNode animeNode = createAnime(animeRs);
			   }
			   else {
				   directorRs.next();
				   GraphNode animeNode = createAnime(animeRs);
				   GraphNode directorNode = createDirector(directorRs);
				   createAnimeDirectorConnection(animeNode,directorNode);
			   }
			   
		   }
		   else {
			   if(!directorRs.isBeforeFirst()){
				   GraphNode animeNode = createAnime(animeRs);
				   createAnimeCVConnections(animeNode,cvCharacterRs);
			   }
			   else {
				   directorRs.next();
				   GraphNode animeNode = createAnime(animeRs);
				   GraphNode directorNode = createDirector(directorRs);
				   createAnimeCVConnections(animeNode,cvCharacterRs);
				   createAnimeDirectorConnection(animeNode,directorNode);
			   }
		   }
		   
		   
	   }

	   
	   
	   // Add nodes and connections from an CV Search to the graph
	   public void drawCVSearch(ResultSet[] rs) throws SQLException{
		   ResultSet cvRs = rs[0];
		   ResultSet animeCharacterRs = rs[1];
		   cvRs.next();
		   GraphNode cvNode = createCV(cvRs);
		   if(animeCharacterRs.isBeforeFirst()){
		   createCVAnimeCharacterConnections(cvNode,animeCharacterRs);}
	   }
	   

// Add nodes and connections from an Director Search to the graph
	public void drawDirectorSearch(ResultSet[] rs) throws SQLException{
		   ResultSet directorRs = rs[0];
		   directorRs.next();
		   
		   GraphNode directorNode = createDirector(directorRs);
		   if(rs[1].isBeforeFirst()){
		   createDirectorAnimeConnections(directorNode, rs[1]);}
	   }
	   

// Add nodes and connections from an Type Search to the graph
	public void drawTypeSearch(ResultSet[] rs) throws SQLException{
		   ResultSet typeRs = rs[0];
		   typeRs.next();
		   GraphNode typeNode = createType(typeRs);
		   if(rs[1].isBeforeFirst()){
		   createTypeAnimeConnections(typeNode, rs[1]);}
		   
	   }
	 

	public Display getDisplay(){
		return display;
	}


	public void clearGraph() {

		// remove all the connections

		Object[] objects = graph.getConnections().toArray() ;

		for ( int x = 0 ; x < objects.length;x++){
			((GraphConnection) objects[x]).dispose() ;
		}

		// remove all the nodes

		objects = graph.getNodes().toArray() ;

		for ( int x = 0 ; x < objects.length;x++){
			((GraphNode) objects[x]).dispose();
		}

		} 
	
 } 