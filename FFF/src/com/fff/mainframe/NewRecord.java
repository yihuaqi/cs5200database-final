package com.fff.mainframe;
/**
 * use a TabbedPane to combine three insertion operations together 
 * */
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.fff.panel.newrecord.NewAnime;
import com.fff.panel.newrecord.NewCV;
import com.fff.panel.newrecord.NewDirector;

public class NewRecord extends JTabbedPane {

	/**
	 * Create the panel.
	 */
	private NewAnime newAnime;
	private NewCV newCV;
	private NewDirector newDirector; 
	public NewRecord() {
		
		setTabPlacement(JTabbedPane.TOP);
		
		newAnime = new NewAnime();
		newAnime.setName("New Anime");
		this.addTab("New Anime", newAnime);
		
		newCV = new NewCV();
		this.addTab("New CV", newCV);
		
		newDirector = new NewDirector();
		newDirector.setName("New Director");
		this.addTab("New Director", newDirector);
		
		this.setSelectedIndex(0);
		
		int index = this.getSelectedIndex();
	}
	

}
