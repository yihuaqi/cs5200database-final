package com.fff.mainframe;
/**
 * main frame 
 * */
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.login.CurrentUserInfo;
import com.fff.login.Login;
import com.fff.panel.NewRecord;
import com.fff.panel.SearchPanel;
import com.fff.panel.ViewRequest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.CardLayout;

import javax.swing.JTabbedPane;

public class TestFrame extends JFrame {

	private JPanel contentPane;
	private SearchPanel searchPane;
	private ViewRequest viewPane;
	private NewRecord newRecord;
	int currentP; //which menu is displayed now
				  // -1, none; 0, view request; 1, add new record; 2,search

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					new Login();
					/**Connect to the database**/
					
						ConnectDatabase.getConnection();
						System.out.println("conncet to mysql");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TestFrame() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		setTitle("FFF Ainime Collection");
		currentP = -1;
		
		//Menu 
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnUser = new JMenu("User");
		menuBar.add(mnUser);
		
		JMenuItem mntmLogOut = new JMenuItem("Log out");
		mnUser.add(mntmLogOut);
		mntmLogOut.setActionCommand("LogOut");
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnUser.add(mntmExit);
		mntmExit.setActionCommand("Exit");
		
		JMenu mnAdmin = new JMenu("Admin");
		menuBar.add(mnAdmin);
		
		JMenuItem mntmViewRequests = new JMenuItem("View Requests");
		if(CurrentUserInfo.currentUserName.equals("root")){
			mnAdmin.add(mntmViewRequests);
			mntmViewRequests.setActionCommand("ViewRequest");
		}
		
		viewPane = new ViewRequest();
		
		JMenuItem mntmAddNewRecord = new JMenuItem("Add New Record");
		mnAdmin.add(mntmAddNewRecord);
		mntmAddNewRecord.setActionCommand("NewRecord");
		newRecord = new NewRecord();
		
		JMenu mnSearch = new JMenu("Search");
		menuBar.add(mnSearch);
		
		
		
		JMenuItem mntmStartSearch = new JMenuItem("Start Search");
		mnSearch.add(mntmStartSearch);
		mntmStartSearch.setActionCommand("StartSearch");		
		searchPane = new SearchPanel();
		
		CardLayout card = new CardLayout(0, 0);
		getContentPane().setLayout(card);
		add(searchPane,"searchP");
		add(newRecord,"newP");
		
		add(viewPane,"viewP");
		
		
		setVisible(true);
		
		MenuItemListener mnListener = new MenuItemListener();
		mntmStartSearch.addActionListener(mnListener);
		mntmAddNewRecord.addActionListener(mnListener);
		mntmViewRequests.addActionListener(mnListener);
		mntmExit.addActionListener(mnListener);
		mntmLogOut.addActionListener(mnListener);
	}

	/*
	* act accordingly to the operation on
	* menu item
	**/
	public class MenuItemListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			String cmd = arg0.getActionCommand();
			System.out.println(cmd);
			CardLayout card = (CardLayout)getContentPane().getLayout();
			if(cmd.equals("StartSearch")){
				card.show(getContentPane(), "searchP");
				//add(searchPane); 
				currentP = 0;
			}	
			else if(cmd.equals("NewRecord")){
				card.show(getContentPane(), "newP");
				//add(newRecord);
				currentP = 1;
			}
			else if(cmd.equals("ViewRequest")){
				card.show(getContentPane(), "viewP");
				//add(viewPane);
				currentP = 2;
			}
			else if(cmd.equals("LogOut")){
				setVisible(false);
				new Login();
				return;
			}
			else if(cmd.equals("Exit")){
				System.exit(0);
			}
			getContentPane().repaint();
			getContentPane().validate();
			repaint();
			validate();
		}

	}
	
	/* remove current panel before changing to the new one
	 * 
	 */
	private void removePanel(){
		switch(currentP){
			case -1:
				break;
			case 0:{
				this.remove(viewPane);
				break;
			}
			case 1:{
				remove(newRecord);
				break;
			}
			case 2:{
				remove(searchPane);
				break;		
			}
			default:
				break;
		}
		repaint();
	}

}
