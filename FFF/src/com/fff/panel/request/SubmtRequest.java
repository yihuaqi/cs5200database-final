package com.fff.panel.request;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.fff.actor.RegManager;
import com.fff.actor.SubmitManager;
import com.fff.panel.request.RegiRequest.OkButtonListener;
import com.fff.panel.request.RegiRequest.RadioListener;
import com.fff.panel.request.RegiRequest.RejectButtonListener;

public class SubmtRequest extends JPanel {

	/**
	 * Create the panel.
	 */
	private String radioStat = "-1";
	ArrayList<String> req;
	SubmitManager rm = new SubmitManager();
	JPanel listPanel = new JPanel();
	JRadioButton[] regs;
	public SubmtRequest() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(50);
		
		JPanel actionArea = new JPanel();
		add(actionArea);
		actionArea.setLayout(new BorderLayout(0, 10));
		
		
		actionArea.add(listPanel, BorderLayout.CENTER);
		listPanel.setLayout(new GridLayout(4, 1, 0, 0));
		
		JRadioButton sub1 = new JRadioButton("show submit request here");
		//listPanel.add(sub1);
		
		req = rm.updateSubmitReq();
		
		regs = new JRadioButton[req.size()];
		ButtonGroup bg = new ButtonGroup();
		for(int i=0; i<req.size(); i++){
			regs[i] = new JRadioButton(req.get(i).substring(0, req.get(i).indexOf(" Operation:")));			
			regs[i].setActionCommand(i+"");
			
			regs[i].addActionListener(new RadioListener());
			bg.add(regs[i]);
			listPanel.add(regs[i]);
		}
		
		JPanel btnPanel = new JPanel();
		actionArea.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new GridLayout(1, 2, 10, 0));
		
		JButton btnOK = new JButton("Accept");
		btnOK.addActionListener(new OkButtonListener());
		btnPanel.add(btnOK);
		
		JButton btnCancel = new JButton("Reject");
		btnCancel.addActionListener(new RejectButtonListener());
		btnPanel.add(btnCancel);

	}
	
	
	class RejectButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(radioStat.equals("-1"))
			{
				JOptionPane.showMessageDialog(SubmtRequest.this, "Plz select one item");
			}
			else{
			// get the current selected request's content, according to the index of selected radio button
			String radioSelected = req.get(Integer.parseInt(radioStat));
			String partcId = radioSelected.split(" ")[2];
			String content = radioSelected.substring(radioSelected.lastIndexOf(":")+2);
			System.out.println(partcId+";"+content);
			System.out.println(partcId);
			JOptionPane.showMessageDialog(SubmtRequest.this, "You have rejected submit request "+partcId);
			rm.deleteFromContentSubmit(Integer.parseInt(partcId));//delete this request from table contentSubmitter
			listPanel.remove(regs[Integer.parseInt(radioStat)]);//remove the processed request from listpanel
			//listPanel.invalidate();
			//listPanel.revalidate();
			listPanel.repaint();
			}
		}
	}
		
	class OkButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(radioStat.equals("-1"))
			{
				JOptionPane.showMessageDialog(SubmtRequest.this, "Plz select one item");
			}
			else{
			// get the current selected request's content, according to the index of selected radio button
			String radioSelected = req.get(Integer.parseInt(radioStat));
			String partcId = radioSelected.split(" ")[2].trim();
			String content = radioSelected.substring(radioSelected.lastIndexOf(":")+2);
			System.out.println(partcId+";"+content);
			
			JOptionPane.showMessageDialog(SubmtRequest.this, "You have accepted submit request "+partcId);
			rm.insertInToDB(content);// insert the new anime into database
			rm.deleteFromContentSubmit(Integer.parseInt(partcId));//delete this request from table contentSubmitter
			listPanel.remove(regs[Integer.parseInt(radioStat)]);//remove the processed request from listpanel			
			listPanel.repaint();
			}
		}						
	}
	class RadioListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			radioStat = e.getActionCommand();
			showDialog("Accept", req.get(Integer.parseInt(radioStat)));
			
		}
		
		private void showDialog(String title,String s) {
   	        JScrollPane scrollPane = new JScrollPane(new JLabel(s));  
   	        
   	        scrollPane.setPreferredSize(new Dimension(500,500));  
   	        Object message = scrollPane;  
   	          
   	   
   	        JTextArea textArea = new JTextArea(s);  
   	        textArea.setLineWrap(true);  
   	        textArea.setWrapStyleWord(true);  
   	        textArea.setMargin(new Insets(5,5,5,5));  
   	        scrollPane.getViewport().setView(textArea);  
   	        
   	        message = scrollPane;  
   	        JOptionPane pane = new JOptionPane(message, JOptionPane.PLAIN_MESSAGE);
   	        
   	        pane.setMessage(message);
   	        
   	        JDialog dialog =pane.createDialog(title);
   	        dialog.setAlwaysOnTop(true);
   	        dialog.setVisible(true);
   	          
			
		}	
	}
	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
	}
	

}
