package com.fff.panel;
/**
 * Create Search Panel
 * */
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;






import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.Anime;
import com.fff.anime.CV;
import com.fff.anime.Director;
import com.fff.login.Login;
import com.fff.zest.GraphManager;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

public class SearchPanel extends JPanel {
	private JTextField textField;
	private ZestResult result;
	final JComboBox  comboBox;
	GraphManager gm;
	
	/**
	 * Create the panel.
	 * A search panel uses card layout so that
	 * it can convert from one sub panel to the other
	 */
	public SearchPanel() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(100);

		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setBorder(new MatteBorder(0, 0, 1, 0, (Color) new Color(0, 0, 0)));
		
		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(20);
		
		comboBox = new JComboBox ();
		comboBox.addItem("by Anime name");
		comboBox.addItem("by Director");
		comboBox.addItem("by CV");
		comboBox.addItem("by Type");
		comboBox.setSelectedIndex(0);
		
		JScrollPane comboScroll = new JScrollPane(comboBox);	
		panel.add(comboBox);	
		result = new ZestResult();
		
		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String data = "";
				gm = new GraphManager();
				if(comboBox.getSelectedIndex() != -1){
					/*
					* conncet to database and execute 
					* query
					*/
					/**Query**/
					try {
						
						
						String keyWord = textField.getText();
					
						int keyIndex = comboBox.getSelectedIndex();
						
						switch (keyIndex) {
						 
						case 0:
							gm.clearGraph();
							//keyWord = "japaneseanime";
							/*
							 * rs[0]:anime
							 * rs[1]:result set of cv
							 * rs[2]:result set of director
							 */
							ResultSetUsedInZest.rs = Anime.query(keyWord);
							 /*
					    	    * drawCVSearch();
					    	    * drawTypeSearch();
					    	    * drawDirectorSearch();
					    	    * ...
					    	   */
							   if(ResultSetUsedInZest.rs[0]==null){
								   JOptionPane.showMessageDialog(SearchPanel.this, "No results found.");
							   }else{
								   ResultSetUsedInZest.rs[0].beforeFirst();
								   if(gm.graph.isDisposed()){
									   gm.initial();
								   }
							   gm.drawAnimeSearch(ResultSetUsedInZest.rs);
					    	   gm.openGraph();}
							break;
						case 1:
							gm.clearGraph();
							//keyWord = "director";
							/*
							 * rs[0]:director
							 * rs[1]:result set of anime x direct
							 */
							ResultSetUsedInZest.rs = Director.query(keyWord);
							if(ResultSetUsedInZest.rs[0]==null){
								   JOptionPane.showMessageDialog(SearchPanel.this, "No results found.");
							   }else{
								   if(gm.graph.isDisposed()){
									   gm.initial();
								   }
							   gm.drawDirectorSearch(ResultSetUsedInZest.rs);
					    	   gm.openGraph();}
					    	   //gm1.openGraph();
							break;
						case 2:
							gm.clearGraph();
							//keyWord = "cv";
							/*
							 * rs[0]:cv
							 * rs[1]:result set of anime x charactor
							 */
							ResultSetUsedInZest.rs = CV.query(keyWord);
							if(ResultSetUsedInZest.rs[0]==null){
								   JOptionPane.showMessageDialog(SearchPanel.this, "No results found.");
							   }else{
								   if(gm.graph.isDisposed()){
									   gm.initial();
								   }
							   gm.drawCVSearch(ResultSetUsedInZest.rs);
					    	   gm.openGraph();}
					    	   //gm2.openGraph();
							break;
						case 3:
							gm.clearGraph();
							//keyWord = "animetype";
							/*
							 * rs[0]:anime type
							 * rs[1]:result set of anime x type
							 */
							String typeId = " ";
							keyWord=keyWord.replace("\'", "\\'");
							ResultSetUsedInZest.rs = new java.sql.ResultSet[2];
							PreparedStatement query[] = new PreparedStatement[2];
							query[0] = ConnectDatabase.getStatement("select * from animetype where type = '"+keyWord+"'");
							ResultSetUsedInZest.rs[0] = query[0].executeQuery();
							if(ResultSetUsedInZest.rs[0].next()){
								 typeId = ResultSetUsedInZest.rs[0].getString(2);	       
							      
							query[1] = ConnectDatabase.getStatement("select * from japaneseanime where japaneseanime.type = '"
									+typeId+"'");
							ResultSetUsedInZest.rs[0].beforeFirst();
							ResultSetUsedInZest.rs[1] = query[1].executeQuery();}
							else {
								ResultSetUsedInZest.rs[0] = null; 
								ResultSetUsedInZest.rs[1] = query[0].executeQuery();
							}
							
							if(ResultSetUsedInZest.rs[0]==null){
								   JOptionPane.showMessageDialog(SearchPanel.this, "No results found.");
							   }else{
								   if(gm.graph.isDisposed()){
									   gm.initial();
								   }
							   gm.drawTypeSearch(ResultSetUsedInZest.rs);
					    	   gm.openGraph();}
					    	//gm3.openGraph();
					    	
							break;
						default:

							break;
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					// result 
				}
			}
			
		});
		
		panel.add(btnSearch);	
	}
}
