package com.fff.login;
/*
* create a login interface
*/
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;

import com.fff.actor.ProcessParticipant;
import com.fff.mainframe.TestFrame;
import com.mysql.jdbc.PreparedStatement;

public class Login extends JFrame{
	private JLabel userLabel;
	private JLabel passLabel;
	private JButton signin;
	private JButton login;
	
	/*
	*create a login frame
	**/
	public Login() {
		setTitle("login FFF anime collection");
		JPanel panel = new LoginPanel();
		panel.setLayout(null);
		getContentPane().add(panel);
		setBounds(300, 200, panel.getWidth(), panel.getHeight());
		userLabel = new JLabel();
		userLabel.setText("User Name: ");
		userLabel.setBounds(100, 135, 200, 18);
		userLabel.setForeground(Color.black);
		panel.add(userLabel);
		final JTextField userName = new JTextField();
		userName.setBounds(200, 135, 200, 18);
		panel.add(userName);
		passLabel = new JLabel();
		passLabel.setText("Password: ");
		passLabel.setBounds(100, 165, 200, 18);
		passLabel.setForeground(Color.black);
		panel.add(passLabel);
		final JPasswordField userPassword = new JPasswordField();
		userPassword.addKeyListener(new KeyAdapter() {
			public void keyPressed(final KeyEvent e) {
				if (e.getKeyCode() == 10)
					login.doClick();
			}
		});
		userPassword.setBounds(200, 165, 200, 18);
		panel.add(userPassword);
		
		login = new JButton();
		login.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				//setVisible(false);
				//new TestFrame();
				String uName = userName.getText().trim();
				String uPswd = new String(userPassword.getPassword());
				System.out.println(uName);
				System.out.println(uPswd);
				if(uName.length()==0||uPswd.length()==0){
					JOptionPane.showMessageDialog(Login.this, "User name or password cannot be empty");
					return;
				}

				/* connect to database 
				 * to find user info and verify user input
				 * */
				ProcessParticipant newUser = new ProcessParticipant(uName,uPswd);
				if(newUser.logInIsValid()){
					CurrentUserInfo.currentUserName = uName; //current user name
					setVisible(false);
					new TestFrame();
				}
				else {
					JOptionPane.showMessageDialog(Login.this, "User name or password is incorrect");
				}
				
				
				
			}
		});
		login.setText("Login");
		login.setBounds(180, 195, 100, 18);
		panel.add(login);
		signin = new JButton();
		signin.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				String uName = userName.getText().trim();
				String uPswd = new String(userPassword.getPassword());
				if(uName.length()==0||uPswd.length()==0){
					JOptionPane.showMessageDialog(Login.this, "User name or password cannot be empty");
					return;
				}
				
				if(uName.equals("root")){
					JOptionPane.showMessageDialog(Login.this, "Invalid name: you cannot regist with a root user's name.");
					return;
				}
				/*************************
				 * connect to database
				 * inert a new record into database
				 * */
				
				ProcessParticipant newUser = new ProcessParticipant(uName,uPswd);
				if(newUser.signInIsValid()){
					newUser.insertInToDB(); //insert this request into table regwaitlist
					JOptionPane.showMessageDialog(Login.this, "Regist request has been submitted.");
				}
				else {
					JOptionPane.showMessageDialog(Login.this, "User name is used");
				}
				
			}
		});
		signin.setText("Regist");
		signin.setBounds(300,195, 100, 18);
		panel.add(signin);
		setVisible(true);
		setResizable(false);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}
