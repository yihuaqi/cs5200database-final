package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

/****************************************
 * Image
 * */
public class Person {

private String name, gender, url;
private int age;
	

/*
 * Person constructor
 * */
	public Person(String name, String gender, int age, String url){
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.url = url;
	}
	
	/*
	 * check whether or not Person exists in database.
	 * */
	public boolean isExisted() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from person");
			rs = query.executeQuery();
			while (rs.next()) {
				if(name.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * insert Person record into table Person.
	 * */
	public void insertInToPerson() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement
					("INSERT INTO person (name, age, gender, img) "
					+ "VALUES"
					+ "('"+name+"',"
					+ "'"+age+"',"
					+ "'"+gender+"',"
					+ "'"+url+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	

}
