package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;

/****************************************
 * Animation
 * */
public class Anime {

	private String name, start, ending, descrpt, type, img, queryString;
	public Anime(){
		
	}
	
	/*
	 * Anime constructor
	 * */
	public Anime(String name, String start, String ending, String descrpt, String type, String img){
		this.name = name;
		this.start = start;
		this.ending = ending;
		this.descrpt = descrpt;
		this.type = type;
		this.img = img;
	}

	/*
	 * check whether or not Anime exists in database.
	 * */
	public boolean isExisted() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from japaneseanime");
			rs = query.executeQuery();
			System.out.println("anime name:"+name);
			while (rs.next()) {
				
				if(name.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	/*
	 * insert Anime record into table SubmitContent.
	 * */
	public void insertInToSubmitContent() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String descrpt2 = descrpt.replace("\'", "\\'");
			String name2 = name.replace("\'", "\\'");
			if(ending.equals(""))
			{
				ending = "NULL";
				queryString = "INSERT INTO japaneseanime (name, start, ending, descrpt, type, img) "
						+ "VALUES"						
						+ "(\\'"+name2+"\\',"
						+ "\\'"+start+"\\',"
						+ ""+ending+","
						+ "\\'"+descrpt2+"\\',"
						+ "\\'"+type+"\\',"
						+ "\\'"+img+"\\')";
			}else
			{
			queryString = "INSERT INTO japaneseanime (name, start, ending, descrpt, type, img) "
					+ "VALUES"						
					+ "(\\'"+name2+"\\',"
					+ "\\'"+start+"\\',"
					+ "\\'"+ending+"\\',"
					+ "\\'"+descrpt2+"\\',"
					+ "\\'"+type+"\\',"
					+ "\\'"+img+"\\')";}
			System.out.println(queryString);
			query = ConnectDatabase.getStatement("INSERT INTO contentsubmitter (content) "
					+ "VALUES"						
					+ "('"+queryString+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	/*
	 * insert Anime record into table Anime.
	 * */
	public void insertInToAnime() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String descrpt2 = descrpt.replace("\'", "\\'");
			String name2 = name.replace("\'", "\\'");
			if(ending.equals(""))
			{
				ending = "NULL";
				query = ConnectDatabase.getStatement("INSERT INTO japaneseanime (name, start, ending, descrpt, type, img) "
						+ "VALUES"						
						+ "('"+name2+"',"
						+ "'"+start+"',"
						+ ""+ending+","
						+ "'"+descrpt2+"',"
						+ "'"+type+"',"
						+ "'"+img+"')");
			}
			else
			{
			query = ConnectDatabase.getStatement("INSERT INTO japaneseanime (name, start, ending, descrpt, type, img) "
					+ "VALUES"						
					+ "('"+name2+"',"
					+ "'"+start+"',"
					+ "'"+ending+"',"
					+ "'"+descrpt2+"',"
					+ "'"+type+"',"
					+ "'"+img+"')");
			}
			//System.out.println(descrpt2);
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	/*
	 * search Anime record according to the given keyword.
	 * */
	public static java.sql.ResultSet[] query(String keyWord){
		java.sql.ResultSet rs[] = new java.sql.ResultSet[3];
		PreparedStatement query[] = new PreparedStatement[3];
		int animeId = 0;
		try {
			keyWord=keyWord.replace("\'", "\\'");
			query[0] = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+keyWord+"'");
			rs[0] = query[0].executeQuery();
			
			if(rs[0].next()){
				animeId = Integer.parseInt(rs[0].getString(1));		
				System.out.println(rs[0].getString(2)+" "+rs[0].getString(3)+" "+rs[0].getString(4)+" "+rs[0].getString(5)+" "+rs[0].getString(6)+" "+rs[0].getString(7));
			
			query[1] = ConnectDatabase.getStatement("select * from person, cv, charactor where charactor.playIn = "
													+animeId+" and charactor.voicedBy = cv.pId and cv.pId = person.id");
			
			query[2] = ConnectDatabase.getStatement("select * from person, director, direct where direct.direct = "
													+animeId+" and direct.isDirectedBy = director.pId and director.pID = person.id");
			
			rs[0].beforeFirst();
			rs[1] = query[1].executeQuery();
			rs[2] = query[2].executeQuery();}
			else {
				rs[0] = null;
				rs[1] = query[0].executeQuery();
				rs[2] = query[0].executeQuery();
			}			
			return rs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}
}
