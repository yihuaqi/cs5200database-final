package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

/****************************************
 * CV
 * */
public class CV {
	public String name;
	public String alias, queryString;
	public String descrpt;
	public CV(){	
	}
	
	/*
	 * CV constructor
	 * */
	public CV(String name, String alias, String descrpt) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.alias = alias;
		this.descrpt =descrpt;
	}
	/*
	 * check whether or not CV exists in database.
	 * */
	public boolean isExisted() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from cv");
			rs = query.executeQuery();
			while (rs.next()) {
				if(alias.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	/*
	 * insert CV record into table CV.
	 * */
	public void insertInToCV() {
		// TODO Auto-generated method stub
		String pId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			
			String name2 = name.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from person where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String alias2 = alias.replace("\'", "\\'");
			String descrpt2 = descrpt.replace("\'", "\\'");
			query = ConnectDatabase.getStatement("INSERT INTO cv (pId, alias, descrpt) "
					+ "VALUES"						
					+ "('"+pId+"',"
					+ "'"+alias2+"',"
					+ "'"+descrpt2+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	/*
	 * insert CV record into table SubmitContent.
	 * */
	public void insertInToSubmitContent() {
		// TODO Auto-generated method stub
		String pId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			String name2 = name.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from person where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String alias2 = alias.replace("\'", "\\'");
			String descrpt2 = descrpt.replace("\'", "\\'");
			queryString = "INSERT INTO cv (pId, alias, descrpt) "
					+ "VALUES"						
					+ "(\\'"+pId+"\\',"
					+ "\\'"+alias2+"\\',"
					+ "\\'"+descrpt2+"\\')";
			System.out.println(queryString);
			query = ConnectDatabase.getStatement("INSERT INTO contentsubmitter (content) "
					+ "VALUES"						
					+ "('"+queryString+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	/*
	 * search CV record according to the given keyword.
	 * */
	public static java.sql.ResultSet[] query(String keyWord){
		java.sql.ResultSet rs[] = new java.sql.ResultSet[2];
		PreparedStatement query[] = new PreparedStatement[2];
		int cvId = 0;
		try {
			keyWord=keyWord.replace("\'", "\\'");
			query[0] = ConnectDatabase.getStatement("select * from person, cv where cv.alias = '"+keyWord+"' and cv.pId = person.id");
			rs[0] = query[0].executeQuery();
			if(rs[0].next()){
				cvId = Integer.parseInt(rs[0].getString(1));			
			
			query[1] = ConnectDatabase.getStatement("select * from japaneseanime, charactor where charactor.voicedBy = "
													+cvId+" and charactor.playIn = japaneseanime.id");
			rs[0].beforeFirst();
			rs[1] = query[1].executeQuery();}
			else {
				rs[0]=null;
				rs[1] = query[0].executeQuery();
			}
			return rs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}

}
