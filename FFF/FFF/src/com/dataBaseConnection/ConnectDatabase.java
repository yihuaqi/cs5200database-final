package com.dataBaseConnection;

import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import com.mysql.jdbc.PreparedStatement;

public class ConnectDatabase {
	static Connection con = null;
	static PreparedStatement stmt = null;
	static Properties pro = null;
	
	public static PreparedStatement getStatement(String sql) throws Exception{
		pro = new Properties();
		pro.load(new FileInputStream("config/configuration.properties"));
		Class.forName(pro.getProperty("dbClassName"));
		con = DriverManager.getConnection(pro.getProperty("dbUrl"));
		stmt = (PreparedStatement) con.prepareStatement(sql);
		return stmt;

	}
	
	public static Connection getConnection() throws Exception{
		pro = new Properties();
		pro.load(new FileInputStream("config/configuration.properties"));
		Class.forName(pro.getProperty("dbClassName"));
		con = DriverManager.getConnection(pro.getProperty("dbUrl"));
		return con;
	}
}
