package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

public class Image {
	private String url;
	
	public Image(String url){
		this.url = url;
	}
	
	public boolean isExisted() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from image");
			rs = query.executeQuery();
			while (rs.next()) {
				if(url.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void insert() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement
					("INSERT INTO image (URL) "
					+ "VALUES"
					+ "('"+url+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

}
