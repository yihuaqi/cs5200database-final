package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

public class Charactor {

	private String cv;
	private String anime, queryString;
	private String descrpt;
	private String url;
	public Charactor(){
		
	}
	public Charactor(String cv, String anime, String descrpt, String url) {
		// TODO Auto-generated constructor stub
		this.cv = cv;
		this.anime = anime;
		this.descrpt =descrpt;
		this.url =url;
	}

	public boolean cvExists() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from cv");
			rs = query.executeQuery();
			while (rs.next()) {
				if(cv.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean animeExists() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from japaneseanime");
			rs = query.executeQuery();
			
			while (rs.next()) {
				
				if(anime.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void insertInToCha() {
		// TODO Auto-generated method stub
		String pId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			String cv2 = cv.replace("\'", "\\'");
			
			queryPerson = ConnectDatabase.getStatement("select * from cv where alias = '"+cv2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String aId = "0";
		
		PreparedStatement queryAnime;
		java.sql.ResultSet rsAnime;
		try {
			String anime2 = anime.replace("\'", "\\'");
			queryAnime = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+anime2+"'");
			rsAnime = queryAnime.executeQuery();
			while (rsAnime.next()) {
				aId = rsAnime.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String descrpt2 = descrpt.replace("\'", "\\'");
			query = ConnectDatabase.getStatement("INSERT INTO charactor (voicedBy, playIn, descrpt) "
					+ "VALUES"						
					+ "('"+pId+"',"
					+ "'"+aId+"',"
					+ "'"+descrpt2+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	public void insertInToSubmitContent() {
		// TODO Auto-generated method stub
		String pId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			String cv2 = cv.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from cv where alias = '"+cv2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String aId = "0";
		
		PreparedStatement queryAnime;
		java.sql.ResultSet rsAnime;
		try {
			String anime2 = anime.replace("\'", "\\'");
			queryAnime = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+anime2+"'");
			rsAnime = queryAnime.executeQuery();
			while (rsAnime.next()) {
				aId = rsAnime.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String descrpt2 = descrpt.replace("\'", "\\'");
			queryString = "INSERT INTO charactor (voicedBy, playIn, descrpt) "
					+ "VALUES"						
					+ "(\\'"+pId+"\\',"
					+ "\\'"+aId+"\\',"
					+ "\\'"+descrpt2+"\\')";
			System.out.println(queryString);
			query = ConnectDatabase.getStatement("INSERT INTO contentsubmitter (content) "
					+ "VALUES"						
					+ "('"+queryString+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	

}
