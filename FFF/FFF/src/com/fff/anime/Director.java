package com.fff.anime;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

public class Director {

	private String name;
	private String alias, queryString;
	private String descrpt;
	private String anime;
	public Director(){
		
	}
	public Director(String name, String alias, String anime, String descrpt) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.alias = alias;
		this.anime = anime;
		this.descrpt =descrpt;
	}
	

	public boolean isExisted() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from director");
			rs = query.executeQuery();
			while (rs.next()) {
				if(alias.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	private boolean directIsExited(int pId, int aId){	
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from direct");
			rs = query.executeQuery();
			while (rs.next()) {
				if(pId==Integer.parseInt(rs.getString(1))&&
						aId==Integer.parseInt(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	public void insertInToDirector() {
		// TODO Auto-generated method stub
		String pId = "0";
		String aId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			String name2 = name.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from person where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			String name2 = anime.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				aId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		PreparedStatement query2;
		java.sql.ResultSet rs;
		try {
			String alias2 = alias.replace("\'", "\\'");
			String descrpt2 = descrpt.replace("\'", "\\'");
			query = ConnectDatabase.getStatement("INSERT INTO director (pId, alias, descrpt) "
					+ "VALUES"						
					+ "('"+pId+"',"
					+ "'"+alias2+"',"
					+ "'"+descrpt2+"')");
			System.out.println(query.toString());
			query2 = ConnectDatabase.getStatement("INSERT INTO direct (isDirectedBy, direct, descrpt) "
					+ "VALUES"						
					+ "('"+pId+"',"
					+ "'"+aId+"',"
					+ "'"+descrpt2+"')");
			query.executeUpdate();
			query2.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	public void insertInToDirect()
	{
		String aId = "0";
		String pId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		
		try {
			String name2 = name.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from person where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			String name2 = anime.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				aId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(!directIsExited(Integer.parseInt(pId), Integer.parseInt(aId))){
		
		PreparedStatement query2;
		
		try {
			
			String descrpt2 = descrpt.replace("\'", "\\'");
			
			query2 = ConnectDatabase.getStatement("INSERT INTO direct (isDirectedBy, direct, descrpt) "
					+ "VALUES"						
					+ "('"+pId+"',"
					+ "'"+aId+"',"
					+ "'"+descrpt2+"')");
			//query.executeUpdate();
			query2.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		}
	}
	public void insertInToSubmitContent() {
		// TODO Auto-generated method stub
		String pId = "0";
		String aId = "0";
		PreparedStatement queryPerson;
		java.sql.ResultSet rsPerson;
		try {
			String name2 = name.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from person where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				pId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			String name2 = anime.replace("\'", "\\'");
			queryPerson = ConnectDatabase.getStatement("select * from japaneseanime where name = '"+name2+"'");
			rsPerson = queryPerson.executeQuery();
			while (rsPerson.next()) {
				aId = rsPerson.getString(1);			
			}
			
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			String alias2 = alias.replace("\'", "\\'");
			String descrpt2 = descrpt.replace("\'", "\\'");
			queryString = "INSERT INTO director (pId, alias, descrpt) "
					+ "VALUES"						
					+ "(\\'"+pId+"\\',"
					+ "\\'"+alias2+"\\',"
					+ "\\'"+descrpt2+"\\')";
			System.out.println(queryString);
			query = ConnectDatabase.getStatement("INSERT INTO contentsubmitter (content) "
					+ "VALUES"						
					+ "('"+queryString+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	public boolean animeExists() {
		// TODO Auto-generated method stub
		
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from japaneseanime");
			rs = query.executeQuery();			
			while (rs.next()) {
				
				if(anime.equals(rs.getString(2))){
					return true;
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static java.sql.ResultSet[] query(String keyWord){
		java.sql.ResultSet rs[] = new java.sql.ResultSet[2];
		PreparedStatement query[] = new PreparedStatement[2];
		int directId = 0;
		try {
			keyWord=keyWord.replace("\'", "\\'");
			query[0] = ConnectDatabase.getStatement("select * from person, director where director.alias = '"+keyWord+"' and director.pID = person.id");
			rs[0] = query[0].executeQuery();
			if(rs[0].next()){
				directId = Integer.parseInt(rs[0].getString(1));			
			
			query[1] = ConnectDatabase.getStatement("select * from japaneseanime, direct where direct.isDirectedBy = "
													+directId+" and direct.direct = japaneseanime.id");
			rs[0].beforeFirst();
			rs[1] = query[1].executeQuery();}
			else {
				rs[0] = null;
				rs[1] = query[0].executeQuery();
			}
		/*	while (rs[0].next()) {
				System.out.println(rs[0].getString(1)+" "+rs[0].getString(2));		
			}
			while (rs[1].next()) {
				System.out.println(rs[1].getString(1)+" "+rs[1].getString(2));	
			}*/
			return rs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}	
	}


}
