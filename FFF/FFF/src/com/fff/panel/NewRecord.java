package com.fff.panel;
/**
 * use a TabbedPane to combine three insertion operations together 
 * */
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.fff.panel.newrecord.NewAnime;
import com.fff.panel.newrecord.NewCV;
import com.fff.panel.newrecord.NewCha;
import com.fff.panel.newrecord.NewDirector;

public class NewRecord extends JTabbedPane {

	/**
	 * Create the panel.
	 */
	private NewAnime newAnime;
	private NewCV newCV;
	private NewDirector newDirector; 
	private NewCha newCha;
	public NewRecord() {
		
		setTabPlacement(JTabbedPane.TOP);
		
		newAnime = new NewAnime();
		newAnime.setName("New Anime");
		this.addTab("New Anime", newAnime);
		
		newCV = new NewCV();
		this.addTab("New CV", newCV);
		
		newDirector = new NewDirector();
		newDirector.setName("New Director");
		this.addTab("New Director", newDirector);
		
		newCha = new NewCha();
		newCha.setName("New Character");
		this.addTab("New Character", newCha);
		
		
		
		this.setSelectedIndex(0);
	}
}
