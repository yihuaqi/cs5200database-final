package com.fff.panel.request;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.actor.RegManager;
import com.fff.panel.newrecord.NewAnime;
import com.mysql.jdbc.PreparedStatement;

public class RegiRequest extends JPanel {

	/**
	 * Create the panel.
	 */
	private String radioStat = "-1";
	ArrayList<String> req;
	RegManager rm = new RegManager();
	JPanel listPanel = new JPanel();
	JRadioButton[] regs;
	public RegiRequest() {
		FlowLayout flowLayout = (FlowLayout) getLayout();
		flowLayout.setVgap(50);
		
		JPanel actionArea = new JPanel();
		add(actionArea);
		actionArea.setLayout(new BorderLayout(0, 0));
		
		
		actionArea.add(listPanel, BorderLayout.CENTER);
		listPanel.setLayout(new GridLayout(8, 1, 10, 10));
		
		JRadioButton reg1 = new JRadioButton("show register request here");
		
		
		//String[] req = rm.updateRegReq();
		req = rm.updateRegReq();
		
		regs = new JRadioButton[req.size()];
		//listPanel.add(reg1);
		ButtonGroup bg = new ButtonGroup();
		for(int i=0; i<req.size(); i++){
			regs[i] = new JRadioButton(req.get(i));
			regs[i].setActionCommand(i+"");
			regs[i].addActionListener(new RadioListener());
			bg.add(regs[i]);
			listPanel.add(regs[i]);
		}
		
		JPanel btnPanel = new JPanel();
		actionArea.add(btnPanel, BorderLayout.SOUTH);
		btnPanel.setLayout(new GridLayout(0, 2, 10, 0));
		
		JButton btnOK = new JButton("Accept");
		btnOK.addActionListener(new OkButtonListener());
		btnPanel.add(btnOK);
		
		JButton btnCancel = new JButton("Reject");
		btnCancel.addActionListener(new RejectButtonListener());
		btnPanel.add(btnCancel);

	}
	
	class RejectButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(radioStat.equals("-1"))
			{
				JOptionPane.showMessageDialog(RegiRequest.this, "Plz select one item");
			}
			else{
			// get the current selected request's content, according to the index of selected radio button
			String radioSelected = req.get(Integer.parseInt(radioStat));
			String userName = radioSelected.split(" ")[1];
			String userPswd = radioSelected.split(" ")[3];
			System.out.println(userName+";"+userPswd);
			JOptionPane.showMessageDialog(RegiRequest.this, "You have rejected "+userName+"'s regist request");
			rm.deleteFromWaitList(userName);//delete this request from table regwaitlist
			listPanel.remove(regs[Integer.parseInt(radioStat)]);//remove the processed request from listpanel
			listPanel.repaint();
			}
		}
	}
		
	class OkButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(radioStat.equals("-1"))
			{
				JOptionPane.showMessageDialog(RegiRequest.this, "Plz select one item");
			}
			else{
			// get the current selected request's content, according to the index of selected radio button
			String radioSelected = req.get(Integer.parseInt(radioStat));
			String userName = radioSelected.split(" ")[1];
			String userPswd = radioSelected.split(" ")[3];
			System.out.println(userName+";"+userPswd);
			JOptionPane.showMessageDialog(RegiRequest.this, "You have accepted "+userName+"'s regist request");
			rm.insertInToDB(userName, userPswd);// insert the new user into database
			rm.deleteFromWaitList(userName);//delete this request from table regwaitlist
			listPanel.remove(regs[Integer.parseInt(radioStat)]);//remove the processed request from listpanel
			listPanel.repaint();
			}
		}
		
	}
	class RadioListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			radioStat = e.getActionCommand();
		}
		
	}

}
