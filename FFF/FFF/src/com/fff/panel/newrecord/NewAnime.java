package com.fff.panel.newrecord;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.MatteBorder;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.Anime;
import com.fff.anime.Image;
import com.fff.login.CurrentUserInfo;
import com.mysql.jdbc.PreparedStatement;

public class NewAnime extends JPanel {
	private JTextField txName;
	private JTextField txStart;
	private JTextField txEnd;
	private JTextField txImg;
	private JTextField txDscrp;
	private JComboBox comboBox;
	private int animeId;
	
	/**
	 * Create the panel.
	 */
	@SuppressWarnings("unchecked")
	public NewAnime() {
		animeId = 5;
		setLayout(new BorderLayout(0, 0));
		
		JPanel titlePanel = new JPanel();
		add(titlePanel, BorderLayout.NORTH);
		
		JLabel lbTitle = new JLabel("New Animation");
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 16));
		titlePanel.add(lbTitle);
		
		JPanel centerPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) centerPanel.getLayout();
		flowLayout.setVgap(50);
		add(centerPanel, BorderLayout.CENTER);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(0, 0, 0)));
		centerPanel.add(inputPanel);
		inputPanel.setLayout(new BorderLayout(10, 10));
		
		JPanel bdPanel = new JPanel();
		inputPanel.add(bdPanel, BorderLayout.CENTER);
		bdPanel.setLayout(new BorderLayout(10, 0));
		
		JPanel lbPanel = new JPanel();
		bdPanel.add(lbPanel, BorderLayout.WEST);
		lbPanel.setLayout(new GridLayout(6, 1, 10, 10));
		
		JLabel lbName = new JLabel("Name: ");
		lbPanel.add(lbName);
		
		JLabel lbType = new JLabel("Type: ");
		lbPanel.add(lbType);
		
		JLabel lbStart = new JLabel("Start Date: ");
		lbPanel.add(lbStart);
		
		JLabel lbEnd = new JLabel("End Date: ");
		lbPanel.add(lbEnd);
		
		JLabel lbImg = new JLabel("Image Path: ");
		lbPanel.add(lbImg);
		
		JLabel lbDscrp = new JLabel("Description: ");
		lbPanel.add(lbDscrp);
		
		JPanel txPanel = new JPanel();
		bdPanel.add(txPanel, BorderLayout.CENTER);
		txPanel.setLayout(new GridLayout(6, 1, 10, 10));
		
		txName = new JTextField();
		txPanel.add(txName);
		txName.setColumns(10);
		
		comboBox = new JComboBox();
		comboBox.setEditable(true);
		txPanel.add(comboBox);
		
		/****************************************
		 * 这里要连数据库才能往combobox里面添加item
		 * */
		try {
			PreparedStatement query = ConnectDatabase.getStatement("select * from animetype");
			java.sql.ResultSet rs = query.executeQuery();
			 while (rs.next()) {
				 comboBox.addItem(rs.getString(2));
			        //System.out.println(rs.getString(2)+" ");       
			      }
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
		txStart = new JTextField();
		txStart.setText("yyyy-mm-dd\r\n");
		txPanel.add(txStart);
		txStart.setColumns(10);
		
		txEnd = new JTextField();
		txEnd.setText("yyyy-mm-dd");
		txPanel.add(txEnd);
		txEnd.setColumns(10);
		
		txImg = new JTextField();
		txPanel.add(txImg);
		txImg.setColumns(10);
		
		txDscrp = new JTextField();
		txPanel.add(txDscrp);
		txDscrp.setColumns(10);
		
		JPanel panel = new JPanel();
		inputPanel.add(panel, BorderLayout.SOUTH);
		
		JButton btnOK = new JButton("Submit");
		panel.add(btnOK);
		btnOK.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(txName.getText().isEmpty() || txStart.getText().isEmpty()){
					JOptionPane.showMessageDialog(NewAnime.this, "Name or Start date cannot be empty！");
					return;
				}
				/*
				 * 来这里也要连数据库哦~~
				 
				 * */
				Image img = new Image(txImg.getText());
				Anime anime = new Anime(txName.getText(), txStart.getText(), txEnd.getText(), txDscrp.getText(), 
						                comboBox.getSelectedItem().toString(), txImg.getText());
				if(img.isExisted())
				{
					if(anime.isExisted())
					{
						JOptionPane.showMessageDialog(NewAnime.this, "Anime has already existed.");
					}
					else {
						if(CurrentUserInfo.currentUserName.equals("root")){
							anime.insertInToAnime();
							JOptionPane.showMessageDialog(NewAnime.this, "New Anime has been created.");
						}
						else{
						anime.insertInToSubmitContent();
						JOptionPane.showMessageDialog(NewAnime.this, "Create Request has been submmitted.");
						}
					}
				}
				else {
					img.insert();
					if(anime.isExisted())
					{
						JOptionPane.showMessageDialog(NewAnime.this, "Anime has already existed.");
					}
					else {
						if(CurrentUserInfo.currentUserName.equals("root")){
							anime.insertInToAnime();
							JOptionPane.showMessageDialog(NewAnime.this, "New Anime has been created.");
						}
						else{
						anime.insertInToSubmitContent();
						JOptionPane.showMessageDialog(NewAnime.this, "Create Request has been submmitted.");
						}
					}
				}
			}
			
		});
		
		JButton btnCancel = new JButton("Cancel");
		panel.add(btnCancel);
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// empty all the field
				 txName.setText("");
				 txName.repaint();
				 txStart.setText("");
				 txStart.repaint();
				 txEnd.setText("");
				 txEnd.repaint();
				 txDscrp.setText("");
				 txDscrp.repaint();
				 txImg.setText("");
				 txImg.repaint();
			}
			
		});

	}

}
