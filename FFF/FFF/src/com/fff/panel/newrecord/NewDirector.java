package com.fff.panel.newrecord;
/**
 * create panel to add information of a Director
 * */
import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.border.MatteBorder;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.CV;
import com.fff.anime.Director;
import com.fff.anime.Image;
import com.fff.anime.Person;
import com.fff.login.CurrentUserInfo;
import com.mysql.jdbc.PreparedStatement;

public class NewDirector extends JPanel {
	private JTextField txName;
	private JTextField txAlias;
	private JTextField txGender;
	private JTextField txAge;
	private JTextField txDirect;
	private JTextField txDscrp;
	private JTextField txImg;
	private int directorId;

	/**
	 * Create the panel.
	 */
	public NewDirector() {
		directorId = 9;
		setLayout(new BorderLayout(0, 0));
		
		JPanel titlePanel = new JPanel();
		add(titlePanel, BorderLayout.NORTH);
		
		JLabel lbDir = new JLabel("Director/Author");
		lbDir.setFont(new Font("Tahoma", Font.BOLD, 15));
		titlePanel.add(lbDir);
		
		JPanel centerPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) centerPanel.getLayout();
		flowLayout.setVgap(50);
		add(centerPanel, BorderLayout.CENTER);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(0, 0, 0)));
		centerPanel.add(inputPanel);
		inputPanel.setLayout(new BorderLayout(10, 10));
		
		JPanel bdPanel = new JPanel();
		inputPanel.add(bdPanel, BorderLayout.CENTER);
		bdPanel.setLayout(new BorderLayout(10, 0));
		
		JPanel lbPanel = new JPanel();
		bdPanel.add(lbPanel, BorderLayout.WEST);
		lbPanel.setLayout(new GridLayout(7, 1, 10, 10));
		
		JLabel lbName = new JLabel("Name: ");
		lbPanel.add(lbName);
		
		JLabel lbAlias = new JLabel("Alias: ");
		lbPanel.add(lbAlias);
		
		JLabel lbGender = new JLabel("Gender: ");
		lbPanel.add(lbGender);
		
		JLabel lbAge = new JLabel("Age: ");
		lbPanel.add(lbAge);
		
		JLabel lbDirect = new JLabel("Directs: ");
		lbPanel.add(lbDirect);
		
		JLabel lbDscrp = new JLabel("Description");
		lbPanel.add(lbDscrp);
		
		JLabel lbImg = new JLabel("Image Path: ");
		lbPanel.add(lbImg);
		
		JPanel txPanel = new JPanel();
		bdPanel.add(txPanel, BorderLayout.CENTER);
		txPanel.setLayout(new GridLayout(7, 1, 10, 10));
		
		txName = new JTextField();
		txPanel.add(txName);
		txName.setColumns(10);
		
		txAlias = new JTextField();
		txPanel.add(txAlias);
		txAlias.setColumns(10);
		
		txGender = new JTextField();
		txPanel.add(txGender);
		txGender.setColumns(10);
		
		txAge = new JTextField();
		txPanel.add(txAge);
		txAge.setColumns(10);
		
		txDirect = new JTextField();
		txPanel.add(txDirect);
		txDirect.setColumns(10);
		
		txDscrp = new JTextField();
		txPanel.add(txDscrp);
		txDscrp.setColumns(10);
		
		txImg = new JTextField();
		txPanel.add(txImg);
		txImg.setColumns(10);
		
		JPanel btnPanel = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) btnPanel.getLayout();
		flowLayout_1.setHgap(20);
		inputPanel.add(btnPanel, BorderLayout.SOUTH);
		
		JButton btnOK = new JButton("Submit");
		btnPanel.add(btnOK);
		btnOK.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(txName.getText().isEmpty() || txDirect.getText().isEmpty()){
					JOptionPane.showMessageDialog(NewDirector.this, "Name or Play in cannot be empty！");
					return;
				}
				/*
				 * 来这里也要连数据库哦~~
				 * */
				Image img = new Image(txImg.getText());
				Person person = new Person(txName.getText(), txGender.getText(), 
		                   45, txImg.getText());
				Director director = new Director(txName.getText(),txAlias.getText(), txDirect.getText(),txDscrp.getText());
				if(director.animeExists()){
				if(img.isExisted())
				{
					createDirector(person, director);
				}
				else {
					img.insert();
					createDirector(person, director);
				}
				}else{
					JOptionPane.showMessageDialog(NewDirector.this, "Anime "+txDirect.getText()+" does not exists, you can create it first");
				}
			}
			
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnPanel.add(btnCancel);
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// empty all the field
				 txName.setText("");
				 txName.repaint();
				 txAlias.setText("");
				 txAlias.repaint();
				 txDirect.setText("");
				 txDirect.repaint();
				 txDscrp.setText("");
				 txDscrp.repaint();
				 txImg.setText("");
				 txImg.repaint();
			}
			
		});

	}
	
	public void createDirector(Person person, Director director){
		if(person.isExisted())
		{
			
				if(CurrentUserInfo.currentUserName.equals("root")){
					if (director.isExisted()) {
						director.insertInToDirect();
						JOptionPane.showMessageDialog(NewDirector.this, "Director infomation has been updated.");
					} 
					else{
					director.insertInToDirector();
					JOptionPane.showMessageDialog(NewDirector.this, "New director has been created.");}
				}
				else{
					if (director.isExisted()) {
						director.insertInToDirect();
						JOptionPane.showMessageDialog(NewDirector.this, "Director infomation has been updated.");
					} 
					else{
					director.insertInToSubmitContent();
				   JOptionPane.showMessageDialog(NewDirector.this, "Create Request has been submmitted.");
				}
			}
		}
		else {
			person.insertInToPerson();
			if(CurrentUserInfo.currentUserName.equals("root")){
				director.insertInToDirector();
				JOptionPane.showMessageDialog(NewDirector.this, "New director has been created.");
			}
			else{
				director.insertInToSubmitContent();
			JOptionPane.showMessageDialog(NewDirector.this, "Create Request has been submmitted.");
			}
		}
	}
}
