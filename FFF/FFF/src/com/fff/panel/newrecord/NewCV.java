package com.fff.panel.newrecord;
/**
 * create panel to add information of a CV
 * */
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JSplitPane;

import java.awt.CardLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.CV;
import com.fff.anime.Image;
import com.fff.anime.Person;
import com.fff.login.CurrentUserInfo;
import com.fff.login.Login;
import com.mysql.jdbc.PreparedStatement;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.border.MatteBorder;

import org.omg.CORBA.PUBLIC_MEMBER;

public class NewCV extends JPanel {
	private JTextField txName;
	private JTextField txAlias;
	private JTextField txGender;
	private JTextField txAge;
	private JTextField txDscrp;
	private JTextField txImg;
	private int cvId;
	/**
	 * Create the panel.
	 */
	public NewCV() {
		cvId =8;
		setLayout(new BorderLayout(0, 0));
		
		JPanel titlePanel = new JPanel();
		add(titlePanel, BorderLayout.NORTH);
		titlePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblCV = new JLabel("CV");
		lblCV.setFont(new Font("Tahoma", Font.BOLD, 16));
		titlePanel.add(lblCV);
		
		JPanel centerPanel = new JPanel();
		FlowLayout fl_centerPanel = (FlowLayout) centerPanel.getLayout();
		fl_centerPanel.setVgap(50);
		add(centerPanel, BorderLayout.CENTER);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(0, 0, 0)));
		centerPanel.add(inputPanel);
		inputPanel.setLayout(new BorderLayout(10, 10));
		
		JPanel bdPanel = new JPanel();
		inputPanel.add(bdPanel);
		bdPanel.setLayout(new BorderLayout(10, 0));
		
		JPanel lbPanel = new JPanel();
		bdPanel.add(lbPanel, BorderLayout.WEST);
		lbPanel.setLayout(new GridLayout(6, 1, 10, 10));
		
		JLabel lbName = new JLabel("Name: ");
		lbPanel.add(lbName);
		lbName.setHorizontalAlignment(SwingConstants.LEFT);
		
		JLabel lbAlias = new JLabel("Alias: ");
		lbPanel.add(lbAlias);
		
		JLabel lbGender = new JLabel("Gender: ");
		lbPanel.add(lbGender);
		
		JLabel lbAge = new JLabel("Age: ");
		lbPanel.add(lbAge);
				
		JLabel lbDscrp = new JLabel("Description: ");
		lbPanel.add(lbDscrp);
		
		JLabel lbImg = new JLabel("Image Path: ");
		lbPanel.add(lbImg);
		
		JPanel txPanel = new JPanel();
		bdPanel.add(txPanel, BorderLayout.CENTER);
		txPanel.setLayout(new GridLayout(6, 1, 0, 10));
		
		txName = new JTextField();
		txPanel.add(txName);
		txName.setColumns(10);
		
		txAlias = new JTextField();
		txPanel.add(txAlias);
		txAlias.setColumns(10);
		
		txGender = new JTextField();
		txPanel.add(txGender);
		txGender.setColumns(10);
		
		txAge = new JTextField();
		txPanel.add(txAge);
		txAge.setColumns(10);
		
		txDscrp = new JTextField();
		txPanel.add(txDscrp);
		txDscrp.setColumns(10);
		
		txImg = new JTextField();
		txPanel.add(txImg);
		txImg.setColumns(10);
		
		
		JPanel btnPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) btnPanel.getLayout();
		flowLayout.setHgap(20);
		inputPanel.add(btnPanel, BorderLayout.SOUTH);
		
		JButton btnOK = new JButton("Submit");
		btnPanel.add(btnOK);
		btnOK.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(txName.getText().isEmpty()){
					JOptionPane.showMessageDialog(NewCV.this, "Name cannot be empty！");
					return;
				}
				/*
				 * 来这里也要连数据库哦~~
				 * */
				Image img = new Image(txImg.getText());
				Person person = new Person(txName.getText(), txGender.getText(), 
						                   22, txImg.getText());
				CV cv = new CV(txName.getText(),txAlias.getText(), txDscrp.getText());
				if(img.isExisted())
				{
					createCV(person, cv);
				}
				else {
					img.insert();
					createCV(person, cv);
				}				
			}
			
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnPanel.add(btnCancel);
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				// empty all the field
				 txName.setText("");
				 txName.repaint();
				 txAlias.setText("");
				 txAlias.repaint();
				 txGender.setText("");
				 txGender.repaint();
				 txAge.setText("");
				 txAge.repaint();
				 txDscrp.setText("");
				 txDscrp.repaint();
				 txImg.setText("");
				 txImg.repaint();
			}
			
		});

	}
	
	public void createCV(Person person, CV cv){
		if(person.isExisted())
		{
			if (cv.isExisted()) {
				JOptionPane.showMessageDialog(NewCV.this, "CV has already existed.");
			} 
			else {
				if(CurrentUserInfo.currentUserName.equals("root")){
					cv.insertInToCV();
					JOptionPane.showMessageDialog(NewCV.this, "New CV has been created.");
				}
				else{
				cv.insertInToSubmitContent();
				JOptionPane.showMessageDialog(NewCV.this, "Create Request has been submmitted.");
				}
			}
		}
		else {
			person.insertInToPerson();
			if(CurrentUserInfo.currentUserName.equals("root")){
				cv.insertInToCV();
				JOptionPane.showMessageDialog(NewCV.this, "New CV has been created.");
			}
			else{
			cv.insertInToSubmitContent();
			JOptionPane.showMessageDialog(NewCV.this, "Create Request has been submmitted.");
			}
		}
	}
}
