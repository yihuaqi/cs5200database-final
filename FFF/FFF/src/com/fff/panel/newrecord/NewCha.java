package com.fff.panel.newrecord;

import javax.swing.JPanel;

import java.awt.BorderLayout;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.MatteBorder;

import com.fff.anime.CV;
import com.fff.anime.Charactor;
import com.fff.anime.Image;
import com.fff.login.CurrentUserInfo;

import java.awt.Color;

public class NewCha extends JPanel {
	private JTextField txCV;
	private JTextField txAni;
	private JTextField txDscrp;
	private JTextField txImg;

	/**
	 * Create the panel.
	 */
	public NewCha() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel titlePanel = new JPanel();
		add(titlePanel, BorderLayout.NORTH);
		
		JLabel lbTitle = new JLabel("New Character");
		lbTitle.setFont(new Font("Tahoma", Font.BOLD, 16));
		titlePanel.add(lbTitle);
		
		JPanel centerPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) centerPanel.getLayout();
		flowLayout.setVgap(50);
		add(centerPanel, BorderLayout.CENTER);
		
		JPanel inputPanel = new JPanel();
		inputPanel.setBorder(new MatteBorder(1, 0, 1, 0, (Color) new Color(0, 0, 0)));
		centerPanel.add(inputPanel);
		inputPanel.setLayout(new BorderLayout(10, 10));
		
		JPanel bdPanel = new JPanel();
		inputPanel.add(bdPanel);
		bdPanel.setLayout(new BorderLayout(10, 0));
		
		JPanel lbPanel = new JPanel();
		bdPanel.add(lbPanel, BorderLayout.WEST);
		lbPanel.setLayout(new GridLayout(4, 1, 10, 10));
		
		JLabel lbCV = new JLabel("Voiced By: ");
		lbPanel.add(lbCV);
		
		JLabel lbAni = new JLabel("Play In: ");
		lbPanel.add(lbAni);
		
		JLabel lbDescrp = new JLabel("Description: ");
		lbPanel.add(lbDescrp);
		
		JLabel lbImg = new JLabel("Image Path: ");
		lbPanel.add(lbImg);
		
		JPanel txPanel = new JPanel();
		bdPanel.add(txPanel, BorderLayout.CENTER);
		txPanel.setLayout(new GridLayout(4, 1, 10, 10));
		
		txCV = new JTextField();
		txPanel.add(txCV);
		txCV.setColumns(10);
		
		txAni = new JTextField();
		txPanel.add(txAni);
		txAni.setColumns(10);
		
		txDscrp = new JTextField();
		txPanel.add(txDscrp);
		txDscrp.setColumns(10);
		
		txImg = new JTextField();
		txPanel.add(txImg);
		txImg.setColumns(10);
		
		JPanel btPanel = new JPanel();
		inputPanel.add(btPanel, BorderLayout.SOUTH);
		btPanel.setLayout(new GridLayout(1, 2, 20, 1));
		
		JButton btnOK = new JButton("Yes");
		btPanel.add(btnOK);
		btnOK.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(txCV.getText().isEmpty() || txAni.getText().isEmpty() || txDscrp.getText().isEmpty()
						){
					JOptionPane.showMessageDialog(NewCha.this, "Name or Play in cannot be empty��");
					return;
				}
				Image img = new Image(txImg.getText());
				Charactor charactor = new Charactor(txCV.getText(), txAni.getText(), 
													txDscrp.getText(), txImg.getText());
				System.out.println("1 anime:"+txAni.getText());
				
				if(img.isExisted())
				{
					createCha(charactor);
				}
				else {
					img.insert();
					createCha(charactor);
				}	
			}
			
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				txCV.setText("");
				txCV.repaint();
				txAni.setText("");
				txAni.repaint();
				txDscrp.setText("");
				txDscrp.repaint();
				txImg.setText("");
				txImg.repaint();
			}
			
		});
		btPanel.add(btnCancel);
		

	}

	public void createCha(Charactor charactor){
		if(charactor.cvExists()){
			if(charactor.animeExists()){
				if(CurrentUserInfo.currentUserName.equals("root")){
					charactor.insertInToCha();
					JOptionPane.showMessageDialog(NewCha.this, "New charactor has been created.");
				}
				else{
					charactor.insertInToSubmitContent();
					JOptionPane.showMessageDialog(NewCha.this, "Create Request has been submmitted.");
				}
			}
			else {
				JOptionPane.showMessageDialog(NewCha.this, "Anime "+txAni.getText()+" does not exists, you can create it first");
			}
		}
		else {
			JOptionPane.showMessageDialog(NewCha.this, "CV "+txCV.getText()+" does not exists, you can create it first");
		}
	}
}
