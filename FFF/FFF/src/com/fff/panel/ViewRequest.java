package com.fff.panel;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.fff.login.CurrentUserInfo;
import com.fff.panel.request.RegiRequest;
import com.fff.panel.request.SubmtRequest;

public class ViewRequest extends JTabbedPane {

	private RegiRequest regiReq;
	private SubmtRequest submtReq;
	/**
	 * Create the panel.
	 */
	public ViewRequest() {
setTabPlacement(JTabbedPane.TOP);
		
		regiReq = new RegiRequest();
		regiReq.setName("View Register Reqest");
		this.addTab("View Register Reqest", regiReq);
		
		submtReq = new SubmtRequest();
		submtReq.setName("View submit Request");
		this.addTab("View submit Request", submtReq);
				
		this.setSelectedIndex(0);
	}

}
