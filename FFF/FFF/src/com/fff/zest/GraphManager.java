package com.fff.zest;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.spi.DirStateFactory.Result;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.eclipse.zest.core.widgets.Graph; 
import org.eclipse.zest.core.widgets.GraphConnection; 
import org.eclipse.zest.core.widgets.GraphNode; 
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles; 
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm; 
import org.eclipse.swt.SWT; 
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FillLayout; 
import org.eclipse.swt.widgets.Display; 
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Shell; 
import org.omg.CORBA.PRIVATE_MEMBER;

import com.dataBaseConnection.ConnectDatabase;
import com.fff.anime.Anime;
import com.fff.anime.CV;
import com.fff.anime.Charactor;
import com.fff.anime.Director;
import com.fff.login.Login;
import com.fff.panel.ResultSetUsedInZest;
//import com.mysql.jdbc.ResultSet;
import com.fff.panel.newrecord.NewAnime;
import com.mysql.jdbc.PreparedStatement;

 public class GraphManager  extends JFrame{
	 final String CVTYPE = "CV";
	 final String DIRECTORTYPE = "Director";
	 final String TYPETYPE = "Type";
	 final String ANIMETYPE = "Anime";
	 Display display;
	  public Shell shell;
	 public Graph graph;
	 public GraphManager() {
		 initial();
	 }
	 
	 public void initial(){
		 display = new Display(); 
         shell = new Shell(display); 
         shell.setText("First Zest Program Demo"); 
         shell.setLayout(new FillLayout()); 
         
         shell.setSize(800, 800); 
         graph = new Graph(shell, SWT.NONE); 
         graph.addSelectionListener(new SelectionAdapter() {
        	 @Override 
        	 public void widgetSelected(SelectionEvent e){
        		 
	    		 List selection = ((Graph) e.widget).getSelection(); 
	    		 
	             if (selection.size() == 1) { 
	            	 
	                    Object o = selection.get(0); 
	                    if (o instanceof GraphNode && !((GraphNode) o).isSelected()) {
	                    	
	                    	GraphNode node = (GraphNode) o;
	                    	//node.s
	                    	String nodeType = (String) node.getData("type");
	                    	String keyword = (String)node.getData("keyword");
	                    	if(nodeType.equals(CVTYPE)){
	                    		
	                    		try {
									drawCVSearch(CV.query(keyword));
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
	                    			
	                    	} else if(nodeType.equals(DIRECTORTYPE)){
	                    		try {
									drawDirectorSearch(Director.query(keyword));
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
	                    		
	                    	} else if(nodeType.equals(ANIMETYPE)){
	                    		try {
									drawAnimeSearch(Anime.query(keyword));
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
	                    	} else if(nodeType.equals(TYPETYPE)){
	                    		try{
		                    		String typeId = " ";
		                    		ResultSet[] rs = new java.sql.ResultSet[2];
									PreparedStatement query[] = new PreparedStatement[2];
									query[0] = ConnectDatabase.getStatement("select * from animetype where type = '"+keyword+"'");
									rs[0] = query[0].executeQuery();
									rs[0].next();
										 typeId = rs[0].getString(2);	       
									      
									query[1] = ConnectDatabase.getStatement("select * from japaneseanime where japaneseanime.type = '"
											+typeId+"'");
									rs[1] = query[1].executeQuery();
		                    		drawTypeSearch(rs);
	                    		}catch (Exception e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
	                    	}
	                    	System.out.println((String)node.getData("image"));
	                    	ImageIcon imageIcon = new ImageIcon("img\\"+(String)node.getData("image"));
	                    	//ImageIcon imageIcon = new ImageIcon("img\\login.jpg");
	                    	
			   	             graph.setLayoutAlgorithm(new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true); 
			   	             /*JOptionPane pane = new JOptionPane((String)node.getData("content"), JOptionPane.PLAIN_MESSAGE);
			   	             
			   	             pane.setIcon(imageIcon);
			   	             
			   	             pane.scrollRectToVisible(getBounds());
			   	             
			   	             JDialog dialog = pane.createDialog((String)node.getData("title"));
			   	             
			   	             
			   	             dialog.setAlwaysOnTop(true);
			   	            
			   	             dialog.setVisible(true);*/
			   	             //String s = "Seid ihr das Essen? 锛堜綘浠槸椋熺墿鍚楋紵锛塡nNein, wir sind der J盲ger! 锛堜笉锛屾垜浠槸鐚庝汉锛侊級\nFeuerroter Pfeil und Bogen. 锛堢孩鑾蹭箣寮撶煝銆傦級\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\n鏃犲悕鐨勭敓鍛戒箣鑺便€€宸叉儴閬懅娈嬭返韪忎竴搴﹀潬鍦扮殑椋為笩銆€姝ｇ劍鎬ヤ互寰呴璧蜂竴鍛冲煁澶寸绁穃n\n涔熶笉浼氭湁浠讳綍鏀瑰彉鑻ユ兂鏀瑰彉銆愮獦杩殑鐜扮姸銆慭n(鐜板湪)銆€鍞湁濂嬭捣鑰屾垬韪忚繃灏镐綋鍓嶈鍢茬瑧鎴戜滑杩涘嚮鎰忓織鐨勭尓鐚″晩瀹剁暅鐨勫畨瀹乗n\n铏氫吉鐨勭箒鑽ｉタ鐙煎彧姹傝嚜鐢?瀹佹涓嶆倲琚洑绂佺殑灞堣颈\n\n姝ｆ槸鍙嶅嚮鐨勯暆鐭㈠湪楂樺鐨勫彟涓€杈规槸灞犳埉鐚庣墿鐨勩€愮寧浜恒€慭n(J盲ger)鍠疯杽鑰屽嚭鐨勩€愭潃鎰忋€?鍐插姩)\n\n姝ｄ笉鏂伡鐑у叾韬互缁?鐏?璐┛榛勬槒鈥斺€旂孩鑾蹭箣寮撶煝\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\n寮曞紦寮€寮︽€ヨ捣鐩磋拷銆€瑕佽鐩爣(瀹?鏃犲鍙€冩斁鍑虹鐭㈢┓杩戒笉鑸峔n\n缁濅笉瀹硅灏嗗叾鏀捐繃灏嗗紦鎷夎嚦婊″紑銆€鍑犺繎宕╂柇鐨勫鸡涓嶆柇灏勫嚭绠煝\n\n鐩磋嚦灏嗐€愮洰鏍囥€?瀹?璇涙潃灞犳潃鐚庣墿鎵€闇€鐨勬棦涓嶆槸銆愬嚩鍣ㄣ€?姝﹀櫒)\n\n涔熶笉鏄妧鏈渶瑕佺殑鍙槸銆€浣犺嚜宸辨晱閿愮殑鏉€鎰廤ir sind der J盲ger锛堟垜浠槸鐚庝汉锛塡n\n濡傜儓鐒颁竴鑸伡鐑紒Wir sind der J盲ger锛堟垜浠槸鐚庝汉锛塡n\n濡傚瘨鍐颁竴鑸喎閰凤紒Wir sind der J盲ger锛堟垜浠槸鐚庝汉锛塡n\n灏嗗繁韬寲浣滅鐭紒Wir sind der J盲ger锛堟垜浠槸鐚庝汉锛塡n\n鍔胯灏嗕竴鍒囨礊绌匡紒Angriff auf die Titanen. 锛堝悜宸ㄤ汉杩涘嚮銆傦級\nDer Junge von Einst wird bald zum Schwert greifen. 锛堝皯骞翠笉涔呬箣鍚庡皢鎵ц捣鍓戙€傦級\nWer nur seine Machtlosigkeit beklagt, kann nichts ver盲ndern. 锛堥偅浜涘洜鏃犲姏鑰屽徆鎭殑浜猴紝鏃犳硶鏀瑰彉浠讳綍鐜扮姸銆傦級\nDer Junge von Einst wird bald das schwarze Schwert ergreifen. 锛堝皯骞存嬁璧蜂簡閭ｆ妸榛戣壊鐨勫墤銆傦級\nHass und Zorn sind eine zweischneidige Klinge. 锛堜粐鎭ㄤ笌鎰ゆ€掓鏄弻鍒冨墤銆傦級\nBald eines Tages wird er dem Schicksal die Z盲hne zeigen. 锛堜笉涔呭悗鐨勪竴澶╀粬灏嗗悜鍛借繍榫囬湶鐛犵墮銆傦級\n鍑℃槸鏀瑰彉涓栫晫涔嬩汉蹇呮槸鑳藉鑸嶅純涔嬩汉杩炲摢鎬曚竴鐐广€愰闄┿€?risk)閮戒笉鎰挎壙鎷呯殑浜哄張鑳藉仛鍒颁簺浠€涔堝憿鈥︹€n鎰氭槯鐨勮鎯炽€€涓嶅疄鐨勮櫄濡勫埌濡備粖灏辫繛椴佽幗鐨勫媷姘旈兘銆庤嚜鐢便€忕殑灏栧叺\n\n鍐虫鐨勬敾鍔胯璧愪簣椋炲鐨勫ゴ闅朵互鑳滃埄锛乗n琚己鍔犵殑鑽掕艾銆€姝ｆ槸杩涘嚮鐨勯暆鐭㈠湪琚墺澶虹殑鍦板钩绾挎槸娓存湜銆庤嚜鐢便€?涓栫晫)鐨勩€愰偅涓€澶╃殑灏戝勾銆?Eren)姘镐笉鍋滄瓏鐨勩€愭潃鎰忋€?鍐插姩)\n\n姝ｄ笉鏂镜铓€鍏惰韩灏嗙传(姝?閫佸線钖勬毊鈥斺€斿啣搴滀箣寮撶煝\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\nAngriff auf Titan. 锛堣繘鍑荤殑宸?浜恒€傦級\nAngriff auf Titan. 锛堣繘鍑荤殑宸ㄤ汉銆傦級\nWieder sind sie hier! 锛堝畠浠張鏉ヤ簡锛侊級";
			   	             showDialog((String)node.getData("type")+":"+(String)node.getData("keyword"),(String)node.getData("content"), imageIcon);


	                    } 

	             }

	            
	             
        	 }

			private void showDialog(String title,String s, ImageIcon imageIcon) {
	   	        JScrollPane scrollPane = new JScrollPane(new JLabel(s));  
	   	        
	   	        scrollPane.setPreferredSize(new Dimension(500,500));  
	   	        Object message = scrollPane;  
	   	          
	   	   
	   	        JTextArea textArea = new JTextArea(s);  
	   	        textArea.setLineWrap(true);  
	   	        textArea.setWrapStyleWord(true);  
	   	        textArea.setMargin(new Insets(5,5,5,5));  
	   	        scrollPane.getViewport().setView(textArea);  
	   	        
	   	        message = scrollPane;  
	   	        JOptionPane pane = new JOptionPane(message, JOptionPane.PLAIN_MESSAGE);
	   	        pane.setIcon(imageIcon);
	   	        pane.setMessage(message);
	   	        
	   	        JDialog dialog =pane.createDialog(title);
	   	        dialog.setAlwaysOnTop(true);
	   	        dialog.setVisible(true);
	   	          
				
			}
         });
         
     }
	 public void openGraph(){
        

	   // 鏄剧ず SWT 
      
			// TODO Auto-generated method stub
			 graph.setLayoutAlgorithm(new RadialLayoutAlgorithm(LayoutStyles.NO_LAYOUT_NODE_RESIZING), true); 
			 shell.open(); 
			 graph.redraw();
			   while (!shell.isDisposed()) { 
			          if(!display.readAndDispatch()) { 
			                 display.sleep();
			        	    
			                 //shell.close();
		          }
			      
			   }
			   System.out.println(graph.isDisposed());
			   display.close();
			   System.out.println(graph.isDisposed());
			   shell.dispose();
			   System.out.println(graph.isDisposed());
		
	   
	 }
	   /*public void createExampleConnection(){

           // 鍒涘缓涓€涓浘褰㈣妭鐐?
           GraphNode startNode = createAnime("寮逛父璁虹牬","鏍″洯鐖辨儏","2013-07-01", "2013-10-01","榛戠櫧鐔婂拰瀛︾敓鐨勬俯棣ㄧ殑鏍″洯鏁呬簨","img\\monokuma.jpg");
           // 鍒涘缓鍙﹀涓€涓浘褰㈣妭鐐?
           GraphNode endNode1 = new GraphNode(graph, SWT.NONE, "End1"); 
           GraphNode endNode2 = new GraphNode(graph, SWT.NONE, "End2");
           GraphNode endNode3 = new GraphNode(graph, SWT.NONE, "End3");
          //GraphNode cvNode = createCV("榛戠櫧鐔?,"瓒呴珮鏍＄骇鐨勫０浼?, "img\\monokuma.jpg");
          //GraphNode dNode = createCV("榛戠櫧鐔?,"瓒呴珮鏍＄骇鐨勬€荤洃鐫?, "img\\monokuma.jpg");

           // 鍒涘缓鑺傜偣鍏宠仈
           new GraphConnection(graph, SWT.NONE, startNode, endNode1); 
           new GraphConnection(graph, SWT.NONE, startNode, endNode2); 
           new GraphConnection(graph, SWT.NONE, startNode, endNode3);
           GraphConnection cvAnimeConnection = new GraphConnection(graph, SWT.NONE, startNode, cvNode);
           new GraphConnection(graph, SWT.NONE, startNode, dNode);
           

           //createCVAnimeConnection(startNode,cvNode,"榛戠櫧鐔?,"瓒呴珮鏍＄骇鐨勬牎闀?,"img\\monokuma.jpg");
           // 璁剧疆甯冨眬绠＄悊鍣?
	   }*/
	   
	   public GraphNode findGraphNode(String keyword,String type){
		   List<GraphNode> nodes = graph.getNodes();
		   for (GraphNode n : nodes) {
			   String k = (String)n.getData("keyword");
			   String t = (String)n.getData("type");
			   if (k.equals(keyword) && type.equals(t)) {
				   return n;
			   }
		   }
		return null;
	   }
	   
	   public boolean hasConnenction(GraphNode n1, GraphNode n2){
		   List<GraphConnection> connections = graph.getConnections();
		   for (GraphConnection graphConnection : connections) {
			GraphNode destination = graphConnection.getDestination();
			GraphNode source = graphConnection.getSource();
			if((destination.equals(n1)&&source.equals(n2))||(destination.equals(n2)&&source.equals(n1))){
				return true;
			}
		}
		   return false;
	   }
	   
	   public GraphNode createCV(ResultSet rs) throws SQLException{
		   return createCV(rs.getString(2),rs.getString(5),rs.getString(8));
	   }
	   
	   public GraphNode createCV(String name,String url,String discription){
		   GraphNode cvNode = findGraphNode(name,CVTYPE);
		   if(cvNode==null){
			   cvNode = new GraphNode(graph, 
					   SWT.NONE);
			   //Image cvImage = new Image(display,imgUrl);
			   //cvNode.setImage(cvImage);
			   //cvNode.setText(name+"\n"+discription);
			   cvNode.setNodeStyle(cvNode.getNodeStyle() & ~ZestStyles.NODES_HIDE_TEXT); 
			   
			   cvNode.setData("title",name);
			   cvNode.setData("content",discription);
			   cvNode.setData("image",url);
			   cvNode.setData("keyword",name);
			   cvNode.setData("type",CVTYPE);
			   
			   Image cvImage = new Image(display,"img\\blank.png");
			   cvNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("CV:\n"+name, 0, 0);
			   //cvNode.setText("title");
			   //cvNode.setSize(50, 50);
			   //cvNode.reskin(NORMAL);
			   
			   //cvNode.setSize(cvNode.getSize().width()+7*gc.getCharWidth("鎿?.toCharArray()[0]), cvNode.getSize().height());
			   System.out.println("cv info: "+cvNode.getText());
		   }
		   return cvNode;

	   }


	public GraphNode createDirector(ResultSet rs) throws SQLException{
		   return createDirector(rs.getString(2),rs.getString(5),rs.getString(8));
	   }
	   
	   public GraphNode createDirector(String name,String url, String discription){
		   GraphNode dNode = findGraphNode(name, DIRECTORTYPE);
		   if(dNode==null){
			   dNode = new GraphNode(graph, SWT.NONE);
			   //Image dImage = new Image(display,imgUrl);
			   //dNode.setImage(dImage);
			   //dNode.setText(name+"\n"+discription);
			   dNode.setData("title",name);
			   dNode.setData("content",discription);
			   dNode.setData("image",url);
			   dNode.setData("keyword",name);
			   dNode.setData("type",DIRECTORTYPE);
			   Image cvImage = new Image(display,"img\\blank.png");
			   dNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("Director:\n"+name, 0, 0);
		   }
		   return dNode;
	   }
	   
	   public GraphNode createType(ResultSet rs) throws SQLException{
		   return createType(rs.getString(2));
	   }
	   
	   public GraphNode createType(String typeName){
		   GraphNode tNode = findGraphNode(typeName, TYPETYPE);
		   if(tNode==null){
		   tNode = new GraphNode(graph, SWT.NONE);
		   //tNode.setText(typeName);
		   tNode.setData("title","TYPE");
		   tNode.setData("content",typeName);
		   tNode.setData("keyword", typeName);
		   tNode.setData("type",TYPETYPE);
		   Image cvImage = new Image(display,"img\\blank.png");
		   tNode.setImage(cvImage);
		   GC gc = new GC(cvImage);
		   gc.drawText("Type:\n"+typeName, 0, 0);
		   }
		   return tNode;
		   
	   }
	   
	   public GraphNode createAnime(ResultSet rs) throws SQLException{
		   
		   String name = rs.getString(2);
		   String startDate = rs.getString(3);
		   String endDate = rs.getString(4); 
		   String discription = rs.getString(5);
		   String type = rs.getString(6);
		   String imgUrl = rs.getString(7);
		   System.out.println(rs.getString(2)+" "+rs.getString(3)+" "+rs.getString(4)+" "+rs.getString(5)+" "+rs.getString(6)+" "+rs.getString(7));
		   return createAnime(name,startDate,endDate,discription,type,imgUrl);
		   
	   }
	   
	   public GraphNode createAnime(String name,String startDate, String endDate,String discription,String type,String imgUrl){
		   GraphNode aNode = findGraphNode(name, ANIMETYPE);
		   if(aNode==null){
			   aNode = new GraphNode(graph, 
					   SWT.NONE);
			   //Image dImage = new Image(display,imgUrl);
			   //dNode.setImage(dImage);
			   if(endDate==null){
				   endDate="present";
			   }
			   String text = name+'\n'+type+'\n'+startDate+"-"+endDate+'\n'+discription;
			   //aNode.setText(text);
			   aNode.setData("title",name);
			   aNode.setData("content",name+'\n'+type+'\n'+startDate+"-"+endDate+'\n'+discription);
			   aNode.setData("keyword",name);
			   aNode.setData("image", imgUrl);
			   aNode.setData("type",ANIMETYPE);
			   Image cvImage = new Image(display,"img\\blank.png");
			   aNode.setImage(cvImage);
			   GC gc = new GC(cvImage);
			   gc.drawText("Anime:\n"+name, 0, 0);
			  
		   }
		   return aNode;
		   
	   }
	   
	   private void createCVAnimeCharacterConnections(GraphNode cvNode,
			ResultSet animeCharacterRs) throws SQLException {
		while(animeCharacterRs.next()){
			GraphNode animeNode = createAnime(animeCharacterRs);
			
			if(!hasConnenction(cvNode, animeNode)){
				createCVAnimeConnection(cvNode, animeNode, animeCharacterRs.getString(10));
			}
		}
		
	}
	   /*
	   public GraphConnection createCVAnimeConnection(GraphNode cvNode,GraphNode animeNode,ResultSet rs) throws SQLException{
		   return createCVAnimeConnection(cvNode, animeNode, rs.getString(0), rs.getString(1), rs.getString(2));
	   }
	   */
	   public GraphConnection createCVAnimeConnection(GraphNode cvNode,GraphNode animeNode, String characterDiscription){
           GraphConnection cvAnimeConnection = new GraphConnection(graph, SWT.NONE, cvNode, animeNode);
           //cvAnimeConnection.setImage(new Image(display,characterURL));
           cvAnimeConnection.setText(characterDiscription);
           //cvAnimeConnection.setImage(new Image(display, ""));
           return cvAnimeConnection;
	   }
	   
       private void createTypeAnimeConnections(GraphNode typeNode,
			ResultSet resultSet) throws SQLException {
		while(resultSet.next()){
			GraphNode animeNode = createAnime(resultSet);
			if(!hasConnenction(typeNode, animeNode)){
				createTypeAnimeConnection(typeNode, animeNode);
			}
		}
		
	}
	   public GraphConnection createTypeAnimeConnection(GraphNode typeNode,GraphNode animeNode){
		   GraphConnection typeAnimeConnection = new GraphConnection(graph, SWT.NONE, typeNode, animeNode);
		   return typeAnimeConnection;
	   }
	   
	   public void createAnimeCVConnections(GraphNode animeNode,ResultSet cvCharacterRS) throws SQLException{
		  
		   while(cvCharacterRS.next()){
			   GraphNode cvNode = createCV(cvCharacterRS);   
			   if(!hasConnenction(animeNode, cvNode)){
				   createAnimeCVConnection(animeNode, cvNode,  cvCharacterRS.getString(11));
			   }
		   }
		   //return createAnimeCVConnection(animeNode, cvNode, cvCharacterRS.getString(7), cvCharacterRS.getString(8), cvCharacterRS.getString(9));
	   }
	   /*
	   public GraphConnection createAnimeCVConnection(GraphNode animeNode,GraphNode cvNode,ResultSet rs) throws SQLException{
		   return createAnimeCVConnection(animeNode, cvNode, rs.getString(0), rs.getString(1), rs.getString(2));
	   }
	   */
	   public void createAnimeCVConnection(GraphNode animeNode,GraphNode cvNode, String characterDiscription){
           GraphConnection animeCVConnection = new GraphConnection(graph, SWT.NONE, animeNode, cvNode);
           //animeCVConnection.setImage(new Image(display,characterURL));
           animeCVConnection.setText(characterDiscription);
           //return animeCVConnection;
	   }

	   
	   public GraphConnection createAnimeDirectorConnection(GraphNode animeNode,GraphNode directorNode){
           GraphConnection animeDirectorConnection = new GraphConnection(graph, SWT.NONE, animeNode, directorNode);
           return animeDirectorConnection;
	   }
	   
	   private void createDirectorAnimeConnections(GraphNode directorNode,
			ResultSet resultSet) throws SQLException {
		while(resultSet.next()){
			GraphNode animeNode = createAnime(resultSet);
			if(!hasConnenction(directorNode, animeNode)){
				createDirectorAnimeConnection(directorNode, animeNode);
			}
		}
		
	}
	   
	   public GraphConnection createDirectorAnimeConnection(GraphNode directorNode,GraphNode animeNode){
           GraphConnection directorAnimeConnection = new GraphConnection(graph, SWT.NONE, directorNode, animeNode);
           return directorAnimeConnection;
	   }
	   
	   public void drawAnimeSearch(ResultSet[] rs) throws SQLException{
		   ResultSet animeRs = rs[0];
		   ResultSet cvCharacterRs = rs[1];
		   ResultSet directorRs = rs[2];
		   animeRs.next();
		   if(!cvCharacterRs.isBeforeFirst())
		   {
			   if(!directorRs.isBeforeFirst()){
				   GraphNode animeNode = createAnime(animeRs);
			   }
			   else {
				   directorRs.next();
				   GraphNode animeNode = createAnime(animeRs);
				   GraphNode directorNode = createDirector(directorRs);
				   createAnimeDirectorConnection(animeNode,directorNode);
			   }
			   
		   }
		   else {
			   if(!directorRs.isBeforeFirst()){
				   GraphNode animeNode = createAnime(animeRs);
				   createAnimeCVConnections(animeNode,cvCharacterRs);
			   }
			   else {
				   directorRs.next();
				   GraphNode animeNode = createAnime(animeRs);
				   GraphNode directorNode = createDirector(directorRs);
				   createAnimeCVConnections(animeNode,cvCharacterRs);
				   createAnimeDirectorConnection(animeNode,directorNode);
			   }
		   }
		   
		   
	   }

	   
	   
	   
	   public void drawCVSearch(ResultSet[] rs) throws SQLException{
		   ResultSet cvRs = rs[0];
		   ResultSet animeCharacterRs = rs[1];
		   cvRs.next();
		   GraphNode cvNode = createCV(cvRs);
		   if(animeCharacterRs.isBeforeFirst()){
		   createCVAnimeCharacterConnections(cvNode,animeCharacterRs);}
	   }
	   


	public void drawDirectorSearch(ResultSet[] rs) throws SQLException{
		   ResultSet directorRs = rs[0];
		   directorRs.next();
		   
		   GraphNode directorNode = createDirector(directorRs);
		   if(rs[1].isBeforeFirst()){
		   createDirectorAnimeConnections(directorNode, rs[1]);}
	   }
	   


	public void drawTypeSearch(ResultSet[] rs) throws SQLException{
		   ResultSet typeRs = rs[0];
		   typeRs.next();
		   GraphNode typeNode = createType(typeRs);
		   if(rs[1].isBeforeFirst()){
		   createTypeAnimeConnections(typeNode, rs[1]);}
		   
	   }
	 

	public Display getDisplay(){
		return display;
	}
	/*public static void main(String[] args) { 
    	   GraphManager gm = new GraphManager();
    	   /*
    	    * drawCVSearch();
    	    * drawTypeSearch();
    	    * drawDirectorSearch();
    	    * ...
    	   
    	   gm.openGraph();

       } */

	public void clearGraph() {

		// remove all the connections

		Object[] objects = graph.getConnections().toArray() ;

		for ( int x = 0 ; x < objects.length;x++){
			((GraphConnection) objects[x]).dispose() ;
		}

		// remove all the nodes

		objects = graph.getNodes().toArray() ;

		for ( int x = 0 ; x < objects.length;x++){
			((GraphNode) objects[x]).dispose();
		}

		} 
	
 } 