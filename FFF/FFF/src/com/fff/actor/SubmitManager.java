package com.fff.actor;

import java.util.ArrayList;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

public class SubmitManager {

	public ArrayList<String> updateSubmitReq() {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from contentsubmitter");
			rs = query.executeQuery();
			
			ArrayList<String> reqs = new ArrayList<String>();
			
			while (rs.next()) {
				reqs.add("Request ID: "+rs.getString(1)+" Operation: "+rs.getString(2));
				
		      }
			return reqs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void deleteFromContentSubmit(int partcId) {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement
					("DELETE FROM contentsubmitter where partcId = "+partcId);
			System.out.println(query.toString());
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

	public void insertInToDB(String queryString) {
		// TODO Auto-generated method stub
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement
					(queryString);
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}

}
