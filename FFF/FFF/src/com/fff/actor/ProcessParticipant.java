package com.fff.actor;

import com.dataBaseConnection.ConnectDatabase;
import com.mysql.jdbc.PreparedStatement;

public class ProcessParticipant {
	private String userName;
	private String userPswd;
	
	/**
	 * Constructor of ProcessParticipant
	 * @param userName
	 * @param userPswd
	 */
	public ProcessParticipant(String userName, String userPswd ){
		this.userName = userName;
		this.userPswd = userPswd;
	}
	
	public boolean logInIsValid(){
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from processParticipant");
			rs = query.executeQuery();
			while (rs.next()) {
				if(userName.equals(rs.getString(2))){
					if(userPswd.equals(rs.getString(3))){
						return true;
					}
					else {
						return false;
					}
				}
		      }
			return false;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean signInIsValid(){
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement("select * from processParticipant");
			rs = query.executeQuery();
			while (rs.next()) {
				if(userName.equals(rs.getString(2))){
					return false;
				}
		      }
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public void insertInToDB(){
		PreparedStatement query;
		java.sql.ResultSet rs;
		try {
			query = ConnectDatabase.getStatement
					("INSERT INTO regwaitlist (usrName, usrPswd) "
					+ "VALUES"
					+ "('"+userName+"',"
					+ "'"+userPswd+"')");
			query.executeUpdate();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
}
