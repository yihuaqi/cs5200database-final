To run the porject on eclipse, first step is to setup database.
1. The project uses mySQL. 
2. The package file dump.zip includes all the tables and existing records of the project,  please use the package to import tables and data into your own schema in mySQL.
3. To connect database with the project, modify file configuration.properties in project directory.

In the login in window:
You can register your account by inputing username and password, and click on register button.  Then you need to login into root account, click "Admin" menu, and click on View Register  Request panel to approve the register quest.
The root account is:
User Name: root
Password: root

Click Search menu and click start search to enter the Search panel.
You can search by Anime name by using the following keywords:
Attack on titan, kill la kill.

You can search by Director by using the following keywords:
Urobuchi Gen, Isayama Hajime

You can search by CV by using the following keywords:
Kana Hanazawa

You can search by type by using the following keywords:
Drama, action

After you click on the search button, a result graph will pop up if there is any result.
Then you can click on the node, it will pop up a dialog showing the detail of the node, and add  anything that is related to the node to the graph.